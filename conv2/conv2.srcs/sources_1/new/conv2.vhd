 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conv2 is
  Port ( 
    clk : in std_logic;
      reset : in std_logic;
      
      read_address : in std_logic_vector(13 downto 0);
      write_address : in std_logic_vector(13 downto 0);
      
      write_en : in std_logic;
      read_en : in std_logic;
      
      wstrb : in std_logic_vector(3 downto 0);
      
      rvalid : out std_logic;
      
      read_data : out std_logic_vector(31 downto 0);
      write_data : in std_logic_vector(31 downto 0)
  );
end conv2;

architecture Behavioral of conv2 is
COMPONENT ram_conv
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

component mult_converter IS
  PORT (
    A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    P : OUT STD_LOGIC_VECTOR(36 DOWNTO 0)
  );
END component;

component mult_b2t IS
  PORT (
    CLK : in std_logic;
    A : IN STD_LOGIC_VECTOR(36 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(42 DOWNTO 0);
    P : OUT STD_LOGIC_VECTOR(79 DOWNTO 0)
  );
END component;

component mult_b2t_2 IS
  PORT (
    A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    P : OUT STD_LOGIC_VECTOR(36 DOWNTO 0)
  );
END component;

signal test : std_logic_vector(31 downto 0);

signal inb_b2t : std_logic_vector(42 downto 0);
signal ina_b2t : std_logic_vector(36 downto 0);
signal result_b2t : std_logic_vector(79 downto 0);

signal ina_b2t_2 : std_logic_vector(31 downto 0);
signal inb_b2t_2 : std_logic_vector(4 downto 0);
signal result_b2t_2 : std_logic_vector(36 downto 0);

signal flag_running : std_logic;
signal flag_stop : std_logic;

signal ctr : unsigned(31 downto 0);

signal mul_A : std_logic_vector(31 downto 0);
signal mul_B : std_logic_vector(4 downto 0);
signal mul_P : std_logic_vector(36 downto 0);

signal flag_start : std_logic;
signal flag_start_t2b : std_logic;

signal flag_mode : std_logic;

signal ram_wea : std_logic_vector(3 downto 0);
signal ram_addra : std_logic_vector(8 downto 0);
signal ram_dina : std_logic_vector(31 downto 0);
signal ram_douta : std_logic_vector(31 downto 0);

signal ram_web : std_logic_vector(3 downto 0);
signal ram_addrb : std_logic_vector(8 downto 0);
signal ram_dinb : std_logic_vector(31 downto 0);
signal ram_doutb : std_logic_vector(31 downto 0);

signal i_data : std_logic_vector(31 downto 0);


begin
   mul_converter_0: mult_converter port map (
       A => mul_A,
       B => mul_B,
       P => mul_P
   );

   mul_converter_2: mult_b2t port map (
       CLK => clk,
       A => ina_b2t,
       B => inb_b2t,
       P => result_b2t
   );
   
   mul_converter_3: mult_b2t_2 port map (
       A => ina_b2t_2,
       B => inb_b2t_2,
       P => result_b2t_2
   );

   inb_b2t <= "000" & x"4bda12f684";
   inb_b2t_2 <= std_logic_vector(to_unsigned(27, 5));
   
   -- always multiply by 27
   mul_b <= "11011";

    ram0: ram_conv port map (
        clka => clk,
        ena => '1',
        wea => ram_wea,
        addra => ram_addra,
        dina => ram_dina,
        douta => ram_douta,

        clkb => clk,
        enb => '1',
        web => ram_web,
        addrb => ram_addrb,
        dinb => ram_dinb,
        doutb => ram_doutb
    );

    process(read_address)
    begin
        if read_address(9) = '1' then
            read_data <= ram_douta;
        else
            read_data <= i_data;
        end if;
    end process;
    
	process(clk)
	   variable state : integer range 0 to 7 := 0;
	begin
        if rising_edge(clk) then
            rvalid <= '1';
            flag_start <= '0';
            flag_start_t2b <= '0';
            flag_stop <= '0';
            
            ram_wea <= x"0";

            -- read from RAM
            if read_address(9) = '1' then
                ram_addra <= read_address(8 downto 0);
            else
            -- read registers
                i_data <= (others => '0');
                case read_address(7 downto 0) is
                    when x"01" =>
                        i_data(0) <= flag_running;
                    when x"02" =>
                        i_data <= std_logic_vector(ctr);
                    when others =>
                end case;
            end if;

            if write_en = '1' then
                -- write to RAM
                if write_address(9) = '1' then
                    ram_addra <= write_address(8 downto 0);
                    -- needed for transfers unequal 32bit words
                    ram_wea <= wstrb; --x"f";
                    ram_dina <= write_data;
                else
                    case write_address(7 downto 0) is
                        when x"01" =>
                            flag_start <= write_data(0);
                            flag_stop <= write_data(1);
                            flag_start_t2b <= write_data(2);
                            flag_mode <= write_data(31);
                        when others =>
                    end case;
                end if;
            end if;
        end if;
    end process;


    process (clk)
    variable state : integer range 0 to 31 := 0 ;
    -- bigint2trytes
    variable bigint_ctr : unsigned(6 downto 0);
    variable tryte_ctr : unsigned(6 downto 0);
	variable r : unsigned(4 downto 0);
	variable J : integer range 0 to 63;
	variable rint : integer range 0 to 31;
	variable trictr : integer range -1 to 2;
	variable packed_data : std_logic_vector(31 downto 0);
	variable packed_ctr : unsigned(8 downto 0);
	variable trits : std_logic_vector(31 downto 0);
    -- trytes2bigint
    variable carry : unsigned(31 downto 0);  -- one bit more
    variable mul_res : unsigned(63 downto 0);
    variable ms_index : integer range 0 to 31;
    variable multmp : unsigned(79 downto 0);
    begin
        if rising_edge(clk) then
            if reset='0' then
                state := 0;
            else
                if flag_running = '1' then
                    ctr <= ctr + 1;
                end if;
                
                if flag_stop = '1' then
                    state := 0;
                end if;
                
                ram_web <= "0000";
                case state is
                    when 0 =>
                        flag_running <= '0';
                        if flag_start = '1' then
                            flag_running <= '1';
                            state := 1;
                            tryte_ctr := to_unsigned(0, tryte_ctr'length);
                            packed_ctr := (others => '0');
                            packed_data := (others => '0');
                            trictr := 0;
                        end if;
                        
                        if flag_start_t2b = '1' then
                            flag_running <= '1';
                            tryte_ctr := to_unsigned(80, tryte_ctr'length);
                            if flag_mode = '1' then
                                packed_ctr := to_unsigned(80, packed_ctr'length);
                            else
                                packed_ctr := to_unsigned(241, packed_ctr'length);
                            end if;
                            packed_data := (others => '0');
                            mul_A <= (others => '0');
                            ms_index := 0;
                            state := 21;
                        end if;
-- bigint to trytes                        
                    when 1 =>
                        bigint_ctr := to_unsigned(11, 7);  -- from top to down
                        r := "00000";
                        state := 2;
                    when 2 =>
                        ram_addrb <= std_logic_vector(resize(bigint_ctr, 9));
                        state := 27;    -- wait for bram data
                    when 27 =>
                        state := 3;
                    when 3 =>
                        ina_b2t <= std_logic_vector(resize(r & unsigned(ram_doutb), 37));
--                        tmp1 <= ram_doutb;
                        state := 26;
                    when 26 =>
                        state := 28; -- pipelining for large mult
                    when 28 =>
                        state := 29;
                    when 29 =>
                        state := 4;
                    when 4 =>
                        multmp := unsigned(result_b2t);
                        multmp := multmp + x"2000000000";  -- round
                        ina_b2t_2 <= std_logic_vector(multmp(74 downto 43));
                        state := 5;
                    when 5 =>
                        ram_dinb <= std_logic_vector(multmp(74 downto 43));
--                            tmp2 <= dout_tdata(31 downto 0);
--                            tmp3 <= x"0000" & dout_tdata(47 downto 32);
                        ram_web <= "1111";
                        r := resize(unsigned(ina_b2t) - unsigned(result_b2t_2),5);
                        bigint_ctr := bigint_ctr - 1;
                        if bigint_ctr = "1111111" then
                            state := 6;
                        else
                            state := 2;
                        end if;
                    when 6 =>
                        rint := to_integer(r);                 
                        case rint is
                            when 0 => trits := x"00ffffff";
                            when 1 => trits := x"00ffff00";
                            when 2 => trits := x"00ffff01";
                            when 3 => trits := x"00ff00ff";
                            when 4 => trits := x"00ff0000";
                            when 5 => trits := x"00ff0001";
                            when 6 => trits := x"00ff01ff";
                            when 7 => trits := x"00ff0100";
                            when 8 => trits := x"00ff0101";
                            when 9 => trits := x"0000ffff";
                            when 10 => trits := x"0000ff00";
                            when 11 => trits := x"0000ff01";
                            when 12 => trits := x"000000ff";
                            when 13 => trits := x"00000000";
                            when 14 => trits := x"00000001";
                            when 15 => trits := x"000001ff";
                            when 16 => trits := x"00000100";
                            when 17 => trits := x"00000101";
                            when 18 => trits := x"0001ffff";
                            when 19 => trits := x"0001ff00";
                            when 20 => trits := x"0001ff01";
                            when 21 => trits := x"000100ff";
                            when 22 => trits := x"00010000";
                            when 23 => trits := x"00010001";
                            when 24 => trits := x"000101ff";
                            when 25 => trits := x"00010100";
                            when 26 => trits := x"00010101";
                            when others => trits := (others =>  '0');
                        end case;

                        if flag_mode = '1' then
                            -- pack 8bit tryte-data to 32bit registers and write to bram
                            -- insert tryte into packed data
                            J := to_integer(packed_ctr(1 downto 0));
                            -- ugly multiplexer-style^^
                            packed_data(J*8+7 downto J*8+0) := std_logic_vector(signed(resize(r, 8))-13);
                            -- increment counter 
                            -- are bits 0 and 1 = "11" -> 4 trytes shifted in data register
                            -- 81th tryte is left-over ... save it too
                            if packed_ctr(1 downto 0) = "11" or (tryte_ctr = 80) then 
                                ram_addrb <= std_logic_vector(resize((packed_ctr(8 downto 2)+32), 9));
                                ram_dinb <= packed_data;
                                ram_web <= "1111";
                                packed_data := x"00000000";
                            end if;
                            packed_ctr := packed_ctr + 1;
                        else
                            -- pack 24bit trit-data to 32bit registers and write to bram
                            for I in 0 to 2 loop
                                -- insert trit into packed data
                                J := to_integer(packed_ctr(1 downto 0));
                                -- ugly multiplexer-style^^
                                packed_data(J*8+7 downto J*8+0) := trits(I*8+7 downto I*8+0);
                                -- increment counter 
                                -- are bits 0 and 1 = "11" -> 4 trits shifted in data register
                                -- 81th tryte is left-over ... save it too
                                if packed_ctr(1 downto 0) = "11" or (I=2 and tryte_ctr = 80) then 
                                    ram_addrb <= std_logic_vector(resize(packed_ctr(8 downto 2)+32,9));
                                    ram_dinb <= packed_data;
                                    ram_web <= "1111";
                                    packed_data := x"00000000";
                                end if;
                                packed_ctr := packed_ctr + 1;
                            end loop;
                        end if;
                        tryte_ctr := tryte_ctr + 1;
                        if tryte_ctr = 81 then
                            state := 7;
                        else
                            state := 1;
                        end if;
                    when 7 =>
                        state := 0;
-- trytes to bigint                         
                    when 10 =>
                        if flag_mode = '1' then
                            trits := (others => '0');
                            trictr := 0;
                        else
                            if packed_ctr = 241 then    -- set 243th trit ... remaining only 2 trits instead of 3 
                                trictr := 1;
                                trits := x"00ff0000";   
                            else
                                trits := (others => '0');
                                trictr := 2;
                            end if;
                        end if;
                        state := 11;
                    when 11 =>
                        ram_addrb <= std_logic_vector(resize(packed_ctr(8 downto 2)+32,9));
                        state := 12;
                    when 12 =>  -- load next data
                        state := 13;    -- wait for tryte from bram
                    when 13 =>
                        packed_data := ram_doutb;
                        
                        J := to_integer(packed_ctr(1 downto 0));
                        trits(trictr*8+7 downto trictr*8+0) := packed_data(J*8+7 downto J*8+0);

                        packed_ctr := packed_ctr - 1;
                        
                        if flag_mode = '1' then
                            carry := x"000000" & unsigned(signed(trits(7 downto 0)) + 13);  -- unbalance
                            
                            if tryte_ctr = 80 then  -- clear 3rd trit
                                if carry <= 8 then 
                                 -- do nothing
                                elsif carry <= 17 then
                                    carry := carry - 9;
                                else
                                    carry := carry - 18;
                                end if;
                            end if;
                            bigint_ctr := (others => '0');
                            state := 15;
                        else
                            trictr := trictr - 1;
                            
                            if trictr = -1 then
                                case trits is
                                    when x"00ffffff" => carry := x"00000000";
                                    when x"00ffff00" => carry := x"00000001";
                                    when x"00ffff01" => carry := x"00000002";
                                    when x"00ff00ff" => carry := x"00000003";
                                    when x"00ff0000" => carry := x"00000004";
                                    when x"00ff0001" => carry := x"00000005";
                                    when x"00ff01ff" => carry := x"00000006";
                                    when x"00ff0100" => carry := x"00000007";
                                    when x"00ff0101" => carry := x"00000008";
                                    when x"0000ffff" => carry := x"00000009";
                                    when x"0000ff00" => carry := x"0000000a";
                                    when x"0000ff01" => carry := x"0000000b";
                                    when x"000000ff" => carry := x"0000000c";
                                    when x"00000000" => carry := x"0000000d";
                                    when x"00000001" => carry := x"0000000e";
                                    when x"000001ff" => carry := x"0000000f";
                                    when x"00000100" => carry := x"00000010";
                                    when x"00000101" => carry := x"00000011";
                                    when x"0001ffff" => carry := x"00000012";
                                    when x"0001ff00" => carry := x"00000013";
                                    when x"0001ff01" => carry := x"00000014";
                                    when x"000100ff" => carry := x"00000015";
                                    when x"00010000" => carry := x"00000016";
                                    when x"00010001" => carry := x"00000017";
                                    when x"000101ff" => carry := x"00000018";
                                    when x"00010100" => carry := x"00000019";
                                    when x"00010101" => carry := x"0000001a";
                                    when others => carry := x"ffffffff";
                                end case;
                                bigint_ctr := (others => '0');
                                state := 15;
                            else
                                if packed_ctr(1 downto 0) = "11" then   -- new data needed
                                    state := 11;
                                end if;
                            end if;
                        end if;
                    when 15 =>
                        ram_addrb <= std_logic_vector(resize(bigint_ctr, 9));
                        state := 16;
                    when 16 =>
                        -- wait for bram data
                        state := 17;
                    when 17 =>
                        mul_A <= ram_doutb;
                        state := 18;
                    when 18 =>
                        mul_res := resize(unsigned(mul_P), 64) + carry;
                        -- save upper 6 bits in carry  
                        carry := mul_res(63 downto 32);        
                        -- needed for first tryte to null bigint when tryte_ctr == 80 
                        ram_addrb <= std_logic_vector(resize(bigint_ctr, 9));
                        ram_dinb <= std_logic_vector(mul_res(31 downto 0));
                        ram_web <= "1111"; 
                        bigint_ctr := bigint_ctr + 1;
--                        if bigint_ctr = 12 then
                        if bigint_ctr > ms_index then
                            state := 19;
                        else 
                            state := 15;
                        end if;
                    when 19 =>
                        if carry /= 0 and bigint_ctr <= 11 then
                            ram_addrb <= std_logic_vector(resize(bigint_ctr, 9));
                            ram_dinb <= std_logic_vector(resize(carry, 32));
                            ram_web <= "1111";
                            ms_index := ms_index + 1;
                        end if;
                        tryte_ctr := tryte_ctr -1;
                        if tryte_ctr = "1111111" then
                            state := 0;
                        else
                            state := 10;
                        end if;
                    when 21 =>  -- init bigints with 0
                        bigint_ctr := (others => '0');
                        state := 22;
                     when 22 =>
                        ram_addrb <= std_logic_vector(resize(bigint_ctr, 9));
                        ram_dinb <= (others => '0');
                        ram_web <= "1111";
                        bigint_ctr := bigint_ctr + 1;
                        if bigint_ctr = 12 then
                            state := 10;
                        end if;
                    when others =>
                        state := 0;
                end case;
            end if;
        end if;
    end process;


end Behavioral;






