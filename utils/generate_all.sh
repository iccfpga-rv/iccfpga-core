#!/bin/bash

# firmware-path
firmware="iccfpga-rv.hex"

# path to implementation files of vivado
impl="../iccfpga/iccfpga.runs/impl_4"


function error {
	echo "error: $1"
	exit 1
}

# removes file if it exists
function remove {
	[ -f "$1" ] && rm "$1"
}

function testyn {
	[[ "$1" != "y" && "$1" != "n" ]] && error "invalid choice. expected y or n"
}	

function def2str {
	case "$1" in
		"y" ) echo "(Y/n)" ;;
		"n" ) echo "(y/N)" ;;
		* ) error "invalid default value" ;;
	esac
}

function ask_default {
	read -p "$2 $( def2str "$3" ): " ASK
	[ -z "$ASK" ] && ASK="$3"
	testyn "$ASK"
	echo "${1}=${ASK}"
}

# replace SEC to TCK in SVF-files
function replace {
        clk=$1           # MHz
        echo "converting SEC to TCK for ${clk}MHz"
        echo "input:  $2"
        echo "output: ${2/.svf/.tmp}"
        echo
        cp "$2" "${2/.svf/.tmp}"
        # calculate TCK based on clock frequency and SEC
        while read line
        do
                secf="$( awk '{ print $2 }' <<< "$line" )"
                before="$( awk -F'.' '{ print $1 }' <<< "$secf" )"
		after="$( awk -F'.' '{ print $2 }' <<< "$secf" | sed 's/^0*//' )"
                [ -z "$after" ] && after=0
                tck=$(( ( 1000000 * $before + $after ) * $clk / 1000000))
                newline="RUNTEST $tck TCK;"
                echo "replace $line with $newline"
                sed "s|$line|$newline|g" -i "${2/.svf/.tmp}"
        done< <(grep 'SEC' "$2" | sort -u )
        echo
}

function usage {
	echo "usage: $0 <flags>"
	echo
	echo "flags:"
	echo "    --encrypted=(y/n)      : flag for encrypting the bitstream"
	echo "    --locked=(y/n)         : flag for locking the debugger"
	echo "    --compile=(y/n)        : compile firmware"
	echo "    --embed-firmware=(y/n) : flag for embedding the firmware"
	echo "    --synth=(y/n)          : flag for resynthesize the system (default y)"
	echo "    --use-spi-slave=(y/n)	 : flag for using SPI slave instead of UART (default n)"
	echo "    --help                 : show help"
	echo
	exit 1
}

function getflag {
	flag="$( awk -F'=' '{ print $1 }' <<< "$1" | tr -d '-'  | tr '[:lower:]' '[:upper:]' )"
	value="$( awk -F'=' '{ print $2 }' <<< "$1" )"
	[[ "$value" == "y" || "$value" == "n" ]] && {
		echo "$flag=\"$value\""
		return 0
	}
	echo "error \"unknown flag value $value\""
}
EMBEDFIRMWARE=""
ENCRYPTED=""
LOCKED=""
COMPILE=""
SYNTH="y"
USESPISLAVE=""

while (( $# )) 
do
	case "$1" in

		--encrypted* | --locked* | --compile* | --embed-firmware* | --synth* | --use-spi-slave* ) eval $( getflag "$1" ) ;;
		--help )	usage ;;
		*) error "unknown parameter" ;;
	esac
        shift
done

#echo $ENCRYPTED
#echo $LOCKED
#echo $COMPILE
#echo $EMBEDFIRMWARE
#echo $SYNTH
#exit

# check if variable set
[ ! -z "$VIVADOPATH" ] && { vivadopath="$VIVADOPATH" ; }

# check if vivado in path
[ -z "$vivadopath" ] && vivadopath="$( which vivado )"

# if not assume some path (which probably only works for me^^)
[ -z "$vivadopath" ] && {
	vivadopath="~/programme/ext/xilinx/Vivado/2018.2/bin"
}

[ ! -f $( eval echo "$vivadopath/vivado" ) ] && {
	error "vivado not found"
}

# sets some values to avoid asking for it
[[ "$SYNTH" == "n" ]] && {
	EMBEDFIRMWARE="n"
	ENCRYPTED="n"
	LOCKED="n"
	COMPILE="n"
}

[[ "$EMBEDFIRMWARE" == "" ]] && {
	eval $( ask_default EMBEDFIRMWARE "embed firmware in bitstream?" "y" )
}

[[ "$ENCRYPTED" == "" ]] && {
	eval $( ask_default ENCRYPTED "build encrypted?" "n" )
}

[[ "$LOCKED" == "" ]] && {
	eval $( ask_default LOCKED "remove debugger?" "n" )
}

[[ "$LOCKED" == "y" && "$EMBEDFIRMWARE" == "n" ]] && {
    error "no debugger without embedded firmware - aborting"
}

FWPATH="../../iccfpga-eclipse/Release"

[[ "$USESPISLAVE" == "y" ]] && {
	echo
	echo "building SPI- instead of UART-interface!"
	FWPATH="../../iccfpga-eclipse/Release_SPI"
}

# relink/create symbolic link to compiled firmware
ln -sf $FWPATH/$firmware $firmware

[[ "$EMBEDFIRMWARE" == "y" ]] && {
	[[ "$COMPILE" == "" ]] && {
		[ ! -f ./iccfpga-rv.coe ] && {
			echo "./iccfpga-rv.coe not found - needs compiling"
			COMPILE="y"
		} || {
			echo "./iccfpga-rv.coe found"
			eval $( ask_default COMPILE "recompile firmware?" "y" )
#			read -p "recompile firmware? (Y/n)" COMPILE
#			[ -z "$COMPILE" ] && COMPILE="y"
#			testyn "$COMPILE"
		}
	}


	[[ "$COMPILE" == "y" ]] && {
		{ which riscv32-unknown-elf-objcopy > /dev/null ; } || {
		        error "riscv32-unknown-elf-objcopy not found in path!"
		}
		echo "trying to compile firmware ..."
		cd $FWPATH
		make clean && make || error "building firmware"
		cd -
	
		# compile bin2mem if needed
		[ ! -f "./bin2mem" ] && {
		        gcc -o bin2mem bin2mem.c
		}
        
        
		# hex to binary
		outfile="${firmware/.hex/.bin}"
		riscv32-unknown-elf-objcopy -I ihex --output-target=binary "$firmware" "${firmware/.hex/.bin}"
	
		# binary to mmm and coe file
		remove "${firmware/.hex/.coe}"
		remove "${firmware/.hex/.mem}"
		./bin2mem "$outfile"
	}
	true
} || {
	# wipe initialization file
	echo -e "memory_initialization_radix=16;\nmemory_initialization_vector=\n00000000;\n" > iccfpga-rv.coe
}

[ ! -f "./iccfpga-rv.coe" ] && {
	error "something went wrong ... No memory initialization file found"
}

# remove files
remove $impl/iccfpga_fpga.svf
remove $impl/iccfpga_spi_flash.svf

echo 


[[ "$SYNTH" == "y" ]] && {
	# generate bd sources extra to patch them afterwards with the lock-setting
	time eval $vivadopath/vivado -mode batch -source generate_sources.tcl ../iccfpga/iccfpga.xpr
	
	[[ "$LOCKED" == "y" ]] && {
	    LOCKFLAG="1"
	} || {
	    LOCKFLAG="0"
	}
	sed "s/\.lock(.)/\.lock(${LOCKFLAG})/g" -i ../iccfpga/iccfpga.srcs/sources_1/bd/design_iccfpga/ip/design_iccfpga_riscvwrapper_0_0/synth/design_iccfpga_riscvwrapper_0_0.v
}


# use C preprocessor to build TCL
FLAGS=""

[[ "$ENCRYPTED" == "y" ]] && FLAGS+="-DENCRYPTED "
[[ "$SYNTH" == "y" ]] && FLAGS+="-DSYNTH "

gcc -E -x c -P $FLAGS generate.template > tmp.tcl

time eval $vivadopath/vivado -mode batch -source tmp.tcl ../iccfpga/iccfpga.xpr


echo "converting svf to xsvf"
echo

# first argument is in Hzz - higher MHz meens longer waiting times if real max clock frequency is constant
# on the raspberry
replace 1500000 $impl/iccfpga_spi_flash.svf
replace 1500000 $impl/iccfpga_fpga.svf

eval $vivadopath/svf_utility -w -xsvf -i $impl/iccfpga_fpga.tmp -o $impl/iccfpga_fpga.xsvf
eval $vivadopath/svf_utility -w -xsvf -i $impl/iccfpga_spi_flash.tmp -o $impl/iccfpga_spi_flash.xsvf

exit 0
