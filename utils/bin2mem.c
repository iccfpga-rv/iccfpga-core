#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char** argv) {
    FILE *ptr;

    printf("reading file %s\n", argv[1]);
    ptr = fopen(argv[1],"rb");  // r for read, b for binary

    FILE * fp;
    fp = fopen ("iccfpga-rv.mem", "w");
    
    FILE* fp_coe;
    fp_coe = fopen("iccfpga-rv.coe", "w");
    
   
    fprintf(fp_coe, "memory_initialization_radix=16;\n");
    fprintf(fp_coe, "memory_initialization_vector=\n");

    fseek(ptr, 0L, SEEK_END);
    uint32_t sz = ftell(ptr);
    fseek(ptr, 0L, SEEK_SET);
    printf("reading %d bytes\n", sz);
    
    uint32_t* buf = malloc(sz);

    uint32_t rd = fread(buf, sz, 1, ptr); 
    
    if (sz % 4) {
        printf("sz mod 4 != 0!\n");
        return 0;
    }
    
    fprintf(fp, "@0\n");    
    for (int i=0;i<sz/4;i++) {
	uint32_t val = buf[i];
	uint32_t valSwapped = ((val & 0x000000ff) << 24) | ((val & 0x0000ff00) << 8) | ((val & 0x00ff0000) >> 8) | ((val & 0xff000000) >> 24);
        fprintf(fp,"%08X\n", valSwapped);
        fprintf(fp_coe,"%08x", val);
        if (i != sz/4-1) {
            fprintf(fp_coe,",\n");
        } else {
            fprintf(fp_coe,";\n");
        }
    }
    
    fclose(ptr);
    fclose(fp);
    fclose(fp_coe);
    
    free(buf);
    printf("iccfpga-rv.mem created successfully\n"); 
    printf("iccfpga-rv.coe created successfully\n"); 

    return 0;
}
