#!/bin/bash

# path to implementation files of vivado
impl="../iccfpga/iccfpga.runs/impl_4"


function error {
	echo "error: $1"
	exit 1
}

# check if variable set
[ ! -z "$VIVADOPATH" ] && { vivadopath="$VIVADOPATH" ; }

# check if vivado in path
[ -z "$vivadopath" ] && vivadopath="$( which vivado )"

# if not assume some path (which probably only works for me^^)
[ -z "$vivadopath" ] && {
	vivadopath="~/programme/ext/xilinx/Vivado/2018.2/bin"
}

[ ! -f $( eval echo "$vivadopath/vivado" ) ] && {
	error "vivado not found"
}


time eval $vivadopath/vivado -mode batch -source clean.tcl ../iccfpga/iccfpga.xpr


exit 0
