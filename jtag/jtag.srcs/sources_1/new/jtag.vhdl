 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity jtag is
    generic (
        LOCK : std_logic := '0'
    ); 
    Port ( 
        tms_ext : in std_logic;
        tdi_ext : in std_logic;
        tck_ext : in std_logic;
        tdo_ext : out std_logic;
        
        tms_rv : out std_logic;
        tdi_rv : out std_logic;
        tck_rv : out std_logic;
        tdo_rv : in std_logic;
        
           lock_out : out std_logic);
end jtag;

architecture Behavioral of jtag is

begin
lock0: if LOCK = '0' generate 
    tms_rv <= tms_ext;
    tdi_rv <= tdi_ext;
    tck_rv <= tck_ext;
    tdo_ext <= tdo_rv;
end generate;

lock1: if LOCK = '1' generate
    tms_rv <= '1';
    tdi_rv <= '1';
    tck_rv <= '1';
    tdo_ext <= 'Z';
end generate;

    lock_out <= LOCK;


end Behavioral;
