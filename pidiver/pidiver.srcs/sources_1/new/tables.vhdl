 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package tables is

type const_sbox_table is array ( 0 to 26) of integer;
constant sbox_table : const_sbox_table := (
    0 => 6,
    1 => 25,
    2 => 17,
    3 => 5,
    4 => 15,
    5 => 10,
    6 => 4,
    7 => 20,
    8 => 24,
    9 => 0,
    10 => 1,
    11 => 2,
    12 => 9,
    13 => 22,
    14 => 26,
    15 => 18,
    16 => 16,
    17 => 14,
    18 => 3,
    19 => 13,
    20 => 23,
    21 => 7,
    22 => 11,
    23 => 12,
    24 => 8,
    25 => 21,
    26 => 19
);

type const_row_table is array(0 to 2) of integer;
constant row_table : const_row_table := (
    0 => 0,
    1 => 3,
    2 => 6
);

type lfsr_type is array(0 to 23, 0 to 26) of std_logic_vector(31 downto 0);

constant const_lfsr : lfsr_type := (
(x"001449aa",x"00014485",x"00081590",x"0021a862",x"0011a428",x"00292a44",x"0008965a",x"00258022",x"00016119",x"00194aa0",x"0012a4a6",x"00196284",x"00106066",x"002115a4",x"001a2a28",x"00141806",x"00144695",x"0011a455",x"00292484",x"001801aa",x"00062586",x"00126101",x"00018864",x"0004a890",x"00266121",x"00068969",x"00151881"),
(x"00048a15",x"00254161",x"00254949",x"00258a89",x"00018619",x"00140020",x"00050105",x"00116021",x"00115924",x"00228404",x"00106188",x"002100a4",x"001845a8",x"002286a6",x"00104648",x"0010a914",x"00095904",x"00298602",x"0002019a",x"0010a680",x"002981a4",x"0012552a",x"00224694",x"00046468",x"001a9651",x"00194086",x"00225486"),
(x"00165988",x"00000aa5",x"00105450",x"00129014",x"001aa684",x"00084166",x"002a0922",x"002a205a",x"0008866a",x"00268a22",x"00210099",x"00084658",x"001aa812",x"00282916",x"0018691a",x"00104996",x"00004504",x"00088100",x"00061042",x"00008021",x"00242120",x"000a2949",x"00102682",x"00059184",x"00145821",x"00205425",x"002696a8"),
(x"00004a29",x"00286020",x"0004190a",x"00106691",x"0011a094",x"001962a4",x"00106166",x"00210524",x"00282208",x"0008180a",x"00114652",x"001028a4",x"002540a4",x"001555a9",x"00000a95",x"00105550",x"00128054",x"0028aa94",x"0020416a",x"002409a8",x"002964a9",x"0004125a",x"00201221",x"002a15a8",x"00096aaa",x"0028a002",x"0010281a"),
(x"00154954",x"00018915",x"00149120",x"00081545",x"0001a102",x"00055850",x"00089111",x"00251662",x"001812a9",x"00151916",x"0004a515",x"00269821",x"00002429",x"00218a60",x"00008898",x"00246560",x"00168659",x"00194805",x"001292a6",x"001a8944",x"002a1a16",x"0029155a",x"001920aa",x"00244146",x"00250829",x"00198069",x"002a6946"),
(x"0004062a",x"00119211",x"001a46a4",x"00026666",x"00096660",x"00082692",x"00009082",x"00062800",x"00120a01",x"000412a4",x"00201961",x"000a6048",x"00289452",x"0021a02a",x"00016958",x"00098590",x"00161462",x"00044225",x"00292161",x"0028654a",x"002485aa",x"00019689",x"00060910",x"00112141",x"00256444",x"00061599",x"00206891"),
(x"00155118",x"00204685",x"0014a558",x"001a9495",x"00296896",x"0024918a",x"00001889",x"00124a50",x"00202994",x"00296128",x"0024498a",x"00054689",x"00092511",x"00102862",x"00254424",x"00251289",x"00085819",x"00295562",x"0015026a",x"00155925",x"00108425",x"0028a2a4",x"0020016a",x"002829a8",x"002860aa",x"0004114a",x"002020a1"),
(x"00190118",x"00266286",x"0016a049",x"001aa445",x"00186586",x"00008106",x"00141010",x"00040605",x"00219161",x"00225148",x"00268098",x"0001a669",x"00250600",x"00291189",x"0009689a",x"00289142",x"0021106a",x"001a4168",x"00228996",x"00101508",x"0006a584",x"00065121",x"00088561",x"00069a42",x"000402a1",x"00111591",x"00062854"),
(x"00220591",x"00286458",x"0014945a",x"0028a065",x"0000258a",x"0001a180",x"00155060",x"00005115",x"002a0620",x"001a528a",x"0011a806",x"0009aa94",x"00148162",x"00291965",x"0029a00a",x"0000685a",x"00295560",x"0005025a",x"00115a21",x"0012a4a4",x"000962a4",x"00186162",x"00200566",x"0028aa28",x"0000480a",x"00185250",x"00112296"),
(x"00255244",x"00046059",x"000a1641",x"00115182",x"00224844",x"00241a98",x"00285929",x"002544aa",x"00251649",x"00281109",x"002924aa",x"0028024a",x"002a182a",x"0029045a",x"001a086a",x"00060866",x"000108a1",x"00008410",x"0004a000",x"00062801",x"00020a21",x"000010a0",x"00220920",x"00182048",x"00140456",x"002588a5",x"0001a499"),
(x"00052050",x"00024611",x"000860a0",x"00281122",x"0019255a",x"001424a6",x"00068285",x"00258451",x"00212809",x"00198a68",x"000a8816",x"00262192",x"001aa249",x"00188406",x"001aa196",x"00189206",x"00190456",x"001608a6",x"00150485",x"00050595",x"00012411",x"00110820",x"00248004",x"00012089",x"00014050",x"00085a10",x"00096582"),
(x"00280142",x"002a286a",x"0008046a",x"00228a62",x"000008a8",x"00204460",x"00149a18",x"00084505",x"002a8122",x"0022915a",x"000292a8",x"00068920",x"00151041",x"00244005",x"002510a9",x"00084959",x"000a4982",x"001a0642",x"000659a6",x"00180801",x"00264226",x"0025a159",x"00225689",x"00266918",x"00160989",x"00252695",x"000a1059"),
(x"00118242",x"001848a4",x"002254a6",x"00165a88",x"00002a25",x"00215860",x"00069018",x"0014a641",x"000a8585",x"00265512",x"00244459",x"00059849",x"0024a241",x"00020489",x"00204550",x"00148858",x"00196955",x"00208506",x"00109118",x"000a1684",x"00015962",x"001a8060",x"002aa916",x"0020191a",x"001a6598",x"000040a6",x"00081580"),
(x"0021a962",x"00119468",x"000a2254",x"00209652",x"002280a8",x"0000a568",x"00159690",x"001808a5",x"00164496",x"00115485",x"00021994",x"00022920",x"00112840",x"00058614",x"00250041",x"00294809",x"0016926a",x"00088125",x"00261122",x"0028a559",x"0000948a",x"0006a940",x"00261651",x"00185089",x"00111856",x"001681a4",x"00099525"),
(x"00151622",x"00041695",x"00109851",x"000a6104",x"00188802",x"001a6216",x"0020aa56",x"00214568",x"00040a98",x"00215521",x"00160668",x"00255a15",x"0014a569",x"000a9685",x"00254952",x"00158969",x"00299885",x"0001aa9a",x"00258100",x"00215049",x"00265048",x"00249059",x"00002249",x"00011400",x"00220500",x"00186148",x"00100496"),
(x"00048484",x"0025a5a1",x"00021259",x"00129200",x"001a8484",x"000a65a6",x"00184202",x"00122096",x"00158284",x"00194465",x"00221a86",x"001a1a48",x"00251216",x"00085659",x"0009a002",x"00046812",x"001a5911",x"00010566",x"00102a20",x"00055884",x"00189aa1",x"00194896",x"00229586",x"00125908",x"00120684",x"00045564",x"00188211"),
(x"002a02a6",x"001a954a",x"000951a6",x"00294902",x"0006825a",x"00058a21",x"001580a1",x"00096515",x"00180822",x"00164016",x"00219295",x"00024a58",x"00282910",x"0008694a",x"002846a2",x"0006a68a",x"00164101",x"00018065",x"00246980",x"00264269",x"0015a909",x"001a9aa5",x"00290856",x"001a89aa",x"001a1486",x"00154986",x"00218605"),
(x"00100168",x"00142994",x"00266125",x"00068929",x"000510a1",x"00004911",x"00284160",x"0016095a",x"000529a5",x"0002a011",x"0005aa20",x"00268881",x"00212619",x"00191828",x"00258446",x"001129a9",x"0005a094",x"001662a1",x"0022a195",x"00219218",x"00224608",x"00146108",x"00020885",x"00200690",x"002894a8",x"0021aa6a",x"00118428"),
(x"00282244",x"0018145a",x"00258066",x"001169a9",x"00018094",x"001466a0",x"0022a2a5",x"00218158",x"00005a98",x"002a6920",x"0014084a",x"00254a65",x"0025a849",x"0022a649",x"00114508",x"00200184",x"00282428",x"00088a4a",x"00264a22",x"00252099",x"000a4259",x"001aaa02",x"00280a96",x"000a554a",x"000941a2",x"002a4542",x"0006496a"),
(x"000908a1",x"00028412",x"00246010",x"00061a09",x"00001621",x"002296a0",x"00024aa8",x"00082420",x"00008a02",x"00144290",x"002126a5",x"00091258",x"00015212",x"000a6690",x"000860a2",x"00081102",x"00112452",x"00150424",x"00250285",x"00095459",x"00291042",x"0019402a",x"00025156",x"000a8220",x"00168182",x"00199445",x"00292186"),
(x"0028600a",x"0014185a",x"00244165",x"00050909",x"0011a2a1",x"00094194",x"001a4622",x"00226296",x"0015a248",x"000a4415",x"000a50a2",x"00199902",x"00099656",x"00150062",x"00154925",x"00118025",x"001861a4",x"00200126",x"00182118",x"00042a86",x"00025241",x"001aa080",x"0008a026",x"00142992",x"00066145",x"001a8401",x"000a6126"),
(x"00288912",x"0012515a",x"000282a4",x"00048560",x"00159a51",x"00288185",x"0012141a",x"00264454",x"00255899",x"00049959",x"00045181",x"00180851",x"00064906",x"00290211",x"001a52aa",x"0011a906",x"00099a14",x"00258542",x"00111969",x"0026a084",x"0002a0a9",x"0025a150",x"00025659",x"001a6000",x"00109806",x"001a6694",x"00006066"),
(x"002915a0",x"00192a6a",x"00149426",x"0018a695",x"00188056",x"002a2aa6",x"0018104a",x"00250066",x"001949a9",x"00028496",x"00146480",x"00229125",x"00029528",x"00165680",x"000069a5",x"00194010",x"00225206",x"0026a458",x"00226459",x"00155448",x"00001155",x"00022200",x"00119880",x"001aaa24",x"00180846",x"00264aa6",x"00152449"),
(x"00260545",x"00196909",x"00108aa6",x"00084444",x"001a9992",x"00191946",x"0015a226",x"000a4955",x"001a0902",x"00262256",x"000a9669",x"00154202",x"00016095",x"00095290",x"00196aa2",x"0020a086",x"00012088",x"00114060",x"00005914",x"000a4500",x"001a4142",x"00028866",x"000428a0",x"00224021",x"00049128",x"00041581",x"0010a951")
);

type const_lane_table is array(0 to 26) of integer;
constant lane_table : const_lane_table := (
    0 => 19,
    1 => 13,
    2 => 21,
    3 => 10,
    4 => 24,
    5 => 15,
    6 => 2,
    7 => 9,
    8 => 3,
    9 => 14,
    10 => 0,
    11 => 6,
    12 => 5,
    13 => 1,
    14 => 25,
    15 => 22,
    16 => 23,
    17 => 20,
    18 => 7,
    19 => 17,
    20 => 26,
    21 => 12,
    22 => 8,
    23 => 18,
    24 => 16,
    25 => 11,
    26 => 4
);

    function sbox_lookup (
        input : std_logic_vector(5 downto 0)
    ) return std_logic_vector;

    function rol_row (
        input : std_logic_vector(17 downto 0);
        n : integer
    ) return std_logic_vector;
    
    function ror_row (
        input : std_logic_vector(17 downto 0);
        n : integer
    ) return std_logic_vector;    

    function rol_lane (
        input : std_logic_vector(53 downto 0);
        n : integer
    ) return std_logic_vector;
	
	function trit_add(
	   a : std_logic_vector(1 downto 0);
	   b : std_logic_vector(1 downto 0)
    ) return std_logic_vector;
    
    function trit_sub(
       a : std_logic_vector(1 downto 0);
       b : std_logic_vector(1 downto 0)
    ) return std_logic_vector;

    function vtrit_add(
		a : std_logic_vector(17 downto 0);
		b : std_logic_vector(17 downto 0)
	) return std_logic_vector;

end tables;


package body tables is

    function rol_row (
        input : std_logic_vector(17 downto 0);
        n : integer
    ) return std_logic_vector is
    begin
        return input(17-n downto 0) & input(17 downto 17-n+1);
    end;
    
    function rol_lane (
        input : std_logic_vector(53 downto 0);
        n : integer
    ) return std_logic_vector is
    begin
        return input(53-n downto 0) & input(53 downto 53-n+1);
    end;
    
    function ror_row (
        input : std_logic_vector(17 downto 0);
        n : integer
    ) return std_logic_vector is
    begin
        return input(n-1 downto 0) & input(17 downto n);
    end;
    
    function trit_add(
       a : std_logic_vector(1 downto 0);
       b : std_logic_vector(1 downto 0)
    ) return std_logic_vector is
    variable tmp : std_logic_vector(3 downto 0);
    begin
        tmp := a & b;
        case tmp is
            when "0000" =>  return "00";
            when "0001" =>  return "01";
            when "0010" =>  return "10";
            when "0100" =>  return "01";
            when "0101" =>  return "10";
            when "0110" =>  return "00";
            when "1000" =>  return "10";
            when "1001" =>  return "00";
            when "1010" =>  return "01";
            when others =>  return "00";
        end case;
    end;

    function trit_sub(
       a : std_logic_vector(1 downto 0);
       b : std_logic_vector(1 downto 0)
    ) return std_logic_vector is
    variable tmp : std_logic_vector(3 downto 0);
    begin
        tmp := a & b;
        case tmp is
            when "0000" =>  return "00";
            when "0001" =>  return "10";
            when "0010" =>  return "01";
            when "0100" =>  return "01";
            when "0101" =>  return "00";
            when "0110" =>  return "10";
            when "1000" =>  return "10";
            when "1001" =>  return "01";
            when "1010" =>  return "00";
            when others =>  return "00";
        end case;
    end;

    function vtrit_add(
		a : std_logic_vector(17 downto 0);
		b : std_logic_vector(17 downto 0)
	) return std_logic_vector is
	variable tmp : std_logic_vector(17 downto 0);
	begin
		for I in 0 to 8 loop
			tmp(I*2+1 downto I*2) := trit_add(a(I*2+1 downto I*2), b(I*2+1 downto I*2));
		end loop;
		return tmp;
	end;


    function sbox_lookup (
        input : std_logic_vector(5 downto 0)
    ) return std_logic_vector is
    begin
        case input is
            when "000000" => return "001000";
            when "000001" => return "000000";
            when "000010" => return "000100";
            when "000100" => return "100100";
            when "000101" => return "000001";
            when "000110" => return "011000";
            when "001000" => return "010100";
            when "001001" => return "000010";
            when "001010" => return "101000";
            when "010000" => return "011010";
            when "010001" => return "010000";
            when "010010" => return "010101";
            when "010100" => return "001001";
            when "010101" => return "010110";
            when "010110" => return "100001";
            when "011000" => return "100010";
            when "011001" => return "011001";
            when "011010" => return "000110";
            when "100000" => return "101001";
            when "100001" => return "100000";
            when "100010" => return "100110";
            when "100100" => return "010001";
            when "100101" => return "101010";
            when "100110" => return "000101";
            when "101000" => return "001010";
            when "101001" => return "100101";
            when "101010" => return "010010";
            when others => return "000000";
        end case;
    end;
end tables;
