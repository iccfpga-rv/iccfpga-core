 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR


library ieee;
 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.index_table.all;
use work.tables.all;
use work.sha512table.all;
 
entity pidiver is
	generic
	(
		VERSION_MAJOR : integer := 1;
		VERSION_MINOR : integer := 1;
		CALC_POW : integer := 0;
		HASH_LENGTH : integer := 243;
		STATE_LENGTH : integer := 729; -- 3 * HASH_LENGTH;
		NONCE_LENGTH : integer := 81; -- HASH_LENGTH / 3;
		NUMBER_OF_ROUNDS : integer := 81;
		PARALLEL : integer := 7;
		INTERN_NONCE_LENGTH : integer	:= 32;
		BITS_MIN_WEIGHT_MAGINUTE_MAX : integer := 26;
		DATA_WIDTH : integer := 9;    -- not used anymore ... keep compatibility
		NONCE_OFFSET : integer := 162;	  -- we hope it nevery changes
		CLOCK_SYNC : integer := 1;        -- synchronize flags between clock domains
		ENABLE_STOP : integer := 1;       -- enable PoW-stop
		-- keccak384
		CALC_KECCAK384 : integer := 0;
		SHA3_384_HASH_SIZE : integer := 48;
		SHA3_384_DIGEST_LENGTH : integer := 48;
		SHA3_MAX_PERMUTATION_SIZE : integer := 25;
		SHA3_MAX_RATE_IN_QWORDS : integer := 24;
		SHA3_384_BLOCK_LENGTH : integer := 104;
		SHA3_384_BLOCK_LENGTH_BITS : integer := 832;
		SHA3_NUM_ROUNDS : integer := 24;
		-- troika
		CALC_TROIKA : integer := 0;
        COLUMNS : integer := 9;
        ROWS : integer := 3;
        SLICES : integer := 27;
        SLICESIZE : integer := 9*3;
        STATESIZE : integer := 9*3*27;
        NUM_SBOXES : integer := 9*3*27/3;
        
        -- sha512
        CALC_SHA512 : integer := 1;
        SHA512_ROUNDS : integer := 80;
        SHA512_USE_BRAM: integer := 0
        		
	);

	port
	(
		clk_fast : in std_logic;
		clk_slow : in std_logic;
		reset_slow : in std_logic;
		reset_fast : in std_logic;

        read_address : in std_logic_vector(13 downto 0);
        write_address : in std_logic_vector(13 downto 0);
        
--		read_en : in std_logic;
		write_en : in std_logic;

		read_data : out std_logic_vector(31 downto 0);
		write_data : in std_logic_vector(31 downto 0);
        rvalid : out std_logic;
		
		led_overflow : out std_logic;
		led_running : out std_logic;
		led_found : out std_logic
	);
	
end pidiver;

architecture behv of pidiver is


subtype state_vector_type is std_logic_vector(PARALLEL-1 downto 0);
--subtype mid_state_vector_type is std_logic_vector(DATA_WIDTH-1 downto 0);
subtype min_weight_magnitude_type is std_logic_vector(BITS_MIN_WEIGHT_MAGINUTE_MAX-1 downto 0);

type curl_state_array is array(integer range <>) of state_vector_type;
--type mid_state_array is array(integer range <>) of mid_state_vector_type;
type min_weight_magnitude_array is array(integer range<>) of min_weight_magnitude_type;

signal curl_state_low : curl_state_array(STATE_LENGTH-1 downto 0);
signal curl_state_high : curl_state_array(STATE_LENGTH-1 downto 0);

subtype data_vector_type is std_logic_vector(31 downto 0);
type data_array is array(integer range <>) of data_vector_type;

signal data : data_array(45 downto 0);
signal curl_hash : data_array(15 downto 0);

signal curl_mid_state_low : std_logic_vector(STATE_LENGTH-1 downto 0);
signal curl_mid_state_high : std_logic_vector(STATE_LENGTH-1 downto 0);

-- following 4 signals synchronized across clock domains
signal flag_running : std_logic_vector(2 downto 0);
signal flag_found : std_logic_vector(2 downto 0);
signal flag_overflow : std_logic_vector(2 downto 0);
signal flag_start : std_logic_vector(2 downto 0);
signal flag_stop : std_logic_vector(2 downto 0);
signal reset_curl : std_logic_vector(2 downto 0);

signal flag_curl_finished : std_logic := '0';

-- saves stop request until core has stopped
signal ff_stop : std_logic;

signal flag_stop_slow : std_logic;
signal flag_start_slow : std_logic;
signal flag_running_fast : std_logic;
signal flag_overflow_fast : std_logic;
signal flag_found_fast : std_logic;



 
-------------------------------------
type state_array is array(integer range <>) of std_logic_vector(31 downto 0);

subtype cell is std_logic_vector(1 downto 0);
type row is array(8 downto 0) of cell;
type slice is array(2 downto 0) of row;
type cuboid is array(26 downto 0) of slice;

type parity is array(26 downto 0) of row;


--signal data : state_array(15 downto 0);
signal hash : state_array(45 downto 0);--state_array(15 downto 0);

signal troika_state : cuboid;

signal test : std_logic_vector(31 downto 0);

signal ctr : unsigned(31 downto 0);

signal flag_troika_running : std_logic;
signal flag_troika_start : std_logic;
signal flag_troika_reset : std_logic;
signal flag_troika_load_rate : std_logic := '0';
signal flag_troika_load_capacity : std_logic := '0';
signal flag_troika_mam_reset : std_logic;

signal troika_num_rounds : integer range 0 to 31;

signal output : std_logic_vector(31 downto 0);



------------------------------------





type binary_nonce_array is array(integer range<>) of unsigned(INTERN_NONCE_LENGTH-1 downto 0);


signal binary_nonce : unsigned(INTERN_NONCE_LENGTH-1 downto 0);	
signal mask : state_vector_type;
signal min_weight_magnitude : min_weight_magnitude_type;

signal i_binary_nonce : unsigned(INTERN_NONCE_LENGTH-1 downto 0);	
signal tmp_weight_magnitude : min_weight_magnitude_array(0 to PARALLEL-1);

signal flag_curl_reset_slow : std_logic;
signal flag_curl_write_slow : std_logic;
signal flag_curl_do_curl_slow : std_logic;

signal flag_curl_reset : std_logic_vector(2 downto 0);
signal flag_curl_write : std_logic_vector(2 downto 0);
signal flag_curl_do_curl : std_logic_vector(2 downto 0);

signal imask : state_vector_type;


-- keccak384
subtype hash_type is std_logic_vector(63 downto 0);
subtype hash_type_unsigned is unsigned(63 downto 0);
type hash_array is array (integer range<>) of hash_type;
type hash_array_unsigned is array (integer range<>) of hash_type_unsigned;



--signal blockdata : std_logic_vector(SHA3_384_BLOCK_LENGTH_BITS-1 downto 0) := (others => '0');
signal fast_digest : std_logic_vector(SHA3_384_HASH_SIZE*8-1 downto 0);

signal flag_keccak384_start : std_logic;
signal flag_keccak384_reset : std_logic;
signal flag_keccak384_running : std_logic;

signal dummy : std_logic_vector(31 downto 0);




-- sha512
signal flag_sha512_running : std_logic;
signal flag_sha512_start : std_logic;
signal flag_sha512_reset : std_logic;

signal sha512_digest : hash_array_unsigned(7 downto 0);
signal sha512_digest_vector : std_logic_vector(511 downto 0);

signal T1 : unsigned(63 downto 0);
signal T2 : unsigned(63 downto 0);


signal T : hash_array_unsigned(7 downto 0);


function rotl64(a : hash_type; n : integer) 
	return hash_type is
begin
	return a(63-n downto 0) & a(63 downto 63-n+1);
end rotl64;


function expand(b : std_logic) 
	return state_vector_type is
begin
	if b = '1' then
		return (others => '1');
	else
		return (others => '0');
	end if;
end expand;


begin
    rvalid <= '1';
    led_running <= flag_running(2);
	led_found <= flag_found(2);
	led_overflow <= flag_overflow(2);
	
	flag_curl_finished <= not flag_running(2);

    -- async read	
	process(clk_slow)
	variable addrptr : integer range 0 to 254;
    variable tmp : integer range 0 to 7;
    variable tmpAddr : integer range 0 to 63;
    variable tmpIdxDst : integer range 0 to 31;
    variable tmpIdxSrc : integer range 0 to 31;
    	
	begin
        if rising_edge(clk_slow) then
            if 1=0 then --reset_slow='0' then
            else
--                read_data <= (others => '0'); doesn't work!
--                if read_address(9) = '1' then
--                  if CALC_TROIKA = 1  then
--                    tmp := to_integer(unsigned(read_address(1 downto 0)));
--                    tmpAddr := to_integer(unsigned(read_address(8 downto 2)));
--                    if (tmpAddr <= 45) then
--                        for I in 0 to 3 loop
--                            tmpIdxSrc := tmp * 8 + I * 2;
--                            tmpIdxDst := I * 8;
--                            read_data <= (others => '0');
--                            read_data(tmpIdxDst + 1 downto tmpIdxDst) <= hash(tmpAddr)(tmpIdxSrc + 1 downto tmpIdxSrc);
--                        end loop;
--                    end if;
--                  end if;  
                if read_address(8) = '1' then -- read keccak
                    addrptr := to_integer(unsigned(read_address(8 downto 0)));
                    if CALC_KECCAK384 = 1 then
                        -- read keccak384 hash
                        if addrptr < 12 then
                            read_data <= fast_digest((addrptr*32)+31 downto (addrptr*32)+0);
                        end if;
                    end if;
                    if CALC_TROIKA = 1 then
                        -- read troika hash                        
                        if addrptr >= 32 and addrptr <= 32 + 45 then --15 then
                            read_data <= hash(addrptr - 32);
                        end if;
                    end if;
                    if CALC_POW = 1 then
                        if addrptr >= 96 and addrptr <= 96+15 then
                            read_data <= curl_hash(addrptr - 96);
                        end if;
                    end if;
                    if CALC_SHA512 = 1 then
                        if addrptr >= 128 and addrptr <= 128+15 then
                            read_data <= sha512_digest_vector(((addrptr-128)*32)+31 downto ((addrptr-128)*32)+0);
                        end if;
                    end if;
                else
                    case read_address is
                        when "00000000000001" =>
                            read_data(3 downto 0) <= flag_curl_finished & flag_overflow(2) & flag_found(2) & flag_running(2);
                            if CALC_POW = 1 then
                                read_data(7 downto 4) <= std_logic_vector(to_unsigned(PARALLEL, 4));
                            else
                                read_data(7 downto 4) <= (others => '0');
                            end if;
                            read_data(8+(PARALLEL-1) downto 8) <= mask;
                            read_data(31 downto 28) <= std_logic_vector(to_unsigned(VERSION_MAJOR, 4));
                            read_data(27 downto 24) <= std_logic_vector(to_unsigned(VERSION_MINOR, 4));
                            read_data(23) <= flag_troika_running;
                            read_data(22) <= flag_keccak384_running;
                            read_data(21) <= flag_sha512_running;
                        when "00000000000010" =>
                            read_data(INTERN_NONCE_LENGTH-1 downto 0) <= std_logic_vector(binary_nonce);
                        when "00000000101010" =>
                            read_data <= dummy;
                        when others => 
                    end case;
                end if;
            end if;
        end if;
    end process;
	

    process(clk_slow)
            variable addrptr : integer range 0 to 127;
            variable tmp : integer range 0 to 7;
            variable tmpAddr : integer range 0 to 63;
            variable tmpIdxDst : integer range 0 to 31;
            variable tmpIdxSrc : integer range 0 to 31;
    begin
        if rising_edge(clk_slow) then
            if 1=0 then --reset_slow='0' then
                min_weight_magnitude <= (others => '0');
                flag_start_slow <= '0';
--                read_data <= x"deadbeef"; -- (others => '0');
                addrptr := 0;
            else
                flag_start_slow <= '0';
                flag_curl_reset_slow <= '0';
                flag_curl_write_slow <= '0';
                flag_curl_do_curl_slow <= '0';
                flag_stop_slow <= '0';
    
                flag_keccak384_start <= '0';
                flag_keccak384_reset <= '0';
                
                flag_troika_start <= '0';
                flag_troika_reset <= '0';
                flag_troika_load_rate <= '0';
                flag_troika_load_capacity <= '0';
                flag_troika_mam_reset <= '0';
                
                flag_sha512_start <= '0';
                flag_sha512_reset <= '0';
                

                if write_en = '1' then
--                    if write_address(9) = '1' then
--                        tmp := to_integer(unsigned(write_address(1 downto 0)));
--                        tmpAddr := to_integer(unsigned(write_address(8 downto 2)));
--                        if (tmpAddr <= 45) then
--                            for I in 0 to 3 loop
--                                tmpIdxDst := tmp * 8 + I * 2;
--                                tmpIdxSrc := I * 8;
--                                data(tmpAddr)(tmpIdxDst + 1 downto tmpIdxDst) <= write_data(tmpIdxSrc + 1 downto tmpIdxSrc);
--                            end loop;
--                        end if;
                    if write_address(7) = '1' then
                        addrptr := to_integer(unsigned(write_address(6 downto 0)));
                        if (addrptr <= 45) then --((HASH_LENGTH*3)/DATA_WIDTH)-1) then
                            data(addrptr) <= write_data;
                        end if;
                    else                
                        case write_address is
                            when "00000000000001" => -- start / stop
                                flag_start_slow <= write_data(0);
                                flag_curl_reset_slow <= write_data(1);
                                flag_curl_write_slow <= write_data(2);
                                flag_curl_do_curl_slow <= write_data(3);
    
                                flag_keccak384_start <= write_data(4);
                                flag_keccak384_reset <= write_data(5);
                                flag_stop_slow <= write_data(6);
                                
                                flag_troika_start <= write_data(7);
                                flag_troika_reset <= write_data(8);
                                flag_troika_load_rate <= write_data(9);
                                flag_troika_load_capacity <= write_data(10);
                                flag_troika_mam_reset <= write_data(11);
                                
                                flag_sha512_start <= write_data(12);
                                flag_sha512_reset <= write_data(13);
                            when "00000000000010" =>
                                min_weight_magnitude <= write_data(BITS_MIN_WEIGHT_MAGINUTE_MAX-1 downto 0);
                            when "00000000000011" =>
                                troika_num_rounds <= to_integer(unsigned(write_data(4 downto 0)));
                            when "00000000010101" =>
                                dummy <= write_data;                
                            when others =>
                        end case;
                   end if;
                end if;
            end if;
        end if;
    end process;
    
-- if synchronization enabled
gen_clock_sync: if CLOCK_SYNC = 1 generate
	-- synchronize curl status to slower clock domain
	process (clk_slow)
	begin
		if rising_edge(clk_slow) then
			if 1=0 then --reset_slow='0' then
				flag_running <= (others => '0');
				flag_found <= (others => '0');
				flag_overflow <= (others => '0');
			else
				flag_running <= flag_running(1 downto 0) & flag_running_fast;
				flag_found <= flag_found(1 downto 0) & flag_found_fast;
				flag_overflow <= flag_overflow(1 downto 0) & flag_overflow_fast;
			end if;
		end if;
	end process;
	
	-- synchronize to faster clock domain
--	process (clk_fast)
--	begin
--	   if rising_edge(clk_slow) then
--	       if reset_slow='0' then
--	           reset_curl <= (others => '0');
--           else
--	           reset_curl <= reset_curl(1 downto 0) & reset_slow;
--	       end if;
--       end if;
--    end process;
    
	process (clk_fast)
	begin
		if rising_edge(clk_fast) then
			if 1=0 then --reset_fast='0' then
				flag_start <= (others => '0');
				flag_stop <= (others => '0');
				
				flag_curl_reset <= (others => '0');
				flag_curl_write <= (others => '0');
				flag_curl_do_curl <= (others => '0');
			else
				flag_start <= flag_start(1 downto 0) & flag_start_slow;
				flag_stop <= flag_stop(1 downto 0) & flag_stop_slow;
				
				flag_curl_reset <= flag_curl_reset(1 downto 0) & flag_curl_reset_slow;
                flag_curl_write <= flag_curl_write(1 downto 0) & flag_curl_write_slow;
                flag_curl_do_curl <= flag_curl_do_curl(1 downto 0) & flag_curl_do_curl_slow;

			end if;
		end if;
	end process;
end generate;

-- if synchronization not enabled
gen_clock_sync_else: if CLOCK_SYNC = 0 generate
    flag_running <= (others => flag_running_fast);
    flag_found <= (others => flag_found_fast);
    flag_overflow <= (others => flag_overflow_fast);
    
    flag_start <= (others => flag_start_slow);
    flag_stop <= (others => flag_stop_slow);
    reset_curl <= (others => reset_slow);
    
    flag_curl_reset <= (others => flag_curl_reset_slow);
    flag_curl_write <= (others => flag_curl_write_slow);
    flag_curl_do_curl <= (others => flag_curl_do_curl_slow);
end generate;

-- if stop enabled
gen_stop: if ENABLE_STOP = 1 generate
    process (clk_fast)
	begin
		if rising_edge(clk_fast) then
			if 1=0 then --reset_fast='0' then --reset_slow='0' then
                ff_stop <= '0';
			else
				if flag_stop(2) = '1' then
					ff_stop <= '1';
				elsif flag_running_fast = '0' then
					ff_stop <= '0';
				end if;
			end if;
		end if;
	end process;
end generate;






gen_sha512: if CALC_SHA512 = 1 generate
    
    process(clk_slow)
        variable state : integer range 0 to 63 := 0;
        variable W : hash_array_unsigned(16 downto 0); 
        variable K : unsigned(63 downto 0);
        variable round : integer range 0 to 127;
        
    begin
        if rising_edge(clk_slow) then
            if state = 0 then
                flag_sha512_running <= '0';
            else
                flag_sha512_running <= '1';
            end if;
            case state is
            when 0 =>
                if flag_sha512_start = '1' then
                    T <= sha512_digest;
                    round := 0;
                    state := 1;
                end if;
                
                if flag_sha512_reset = '1' then
                    sha512_digest(0) <= x"6a09e667f3bcc908";
                    sha512_digest(1) <= x"bb67ae8584caa73b";
                    sha512_digest(2) <= x"3c6ef372fe94f82b";
                    sha512_digest(3) <= x"a54ff53a5f1d36f1";
                    sha512_digest(4) <= x"510e527fade682d1";
                    sha512_digest(5) <= x"9b05688c2b3e6c1f";
                    sha512_digest(6) <= x"1f83d9abfb41bd6b";
                    sha512_digest(7) <= x"5be0cd19137e2179";

                end if;
            when 1 =>
                for i in 0 to 15 loop
                    W(I) := le2be(unsigned(data(2*i+1)) & unsigned(data(2*i+0)));
                end loop;
                -- important to initialize element 16
                W(16) := Gamma1(W(14)) + W(9) + Gamma0(W(1)) + W(0);
                
                if SHA512_USE_BRAM = 1 then
                    K := sha512_k(0);
                end if;
                
                state := 2; 
            when 2 =>
                -- synthesis tool uses BRAM automatically when using the right code patterns
                if SHA512_USE_BRAM = 1 then 
                    T1 <= T(7) + Sigma1(T(4)) + Ch(T(4),T(5),T(6)) + W(0) + K;
                    K := sha512_k(round+1);
                else
                    T1 <= T(7) + Sigma1(T(4)) + Ch(T(4),T(5),T(6)) + W(0) + sha512_K(round);
                end if;
                
                T2 <= Sigma0(T(0)) + Maj(T(0),T(1),T(2));
                state := 3;
            when 3 =>
                T(7 downto 0) <= T(6 downto 4) & unsigned(T(3) + T1) & T(2 downto 0) & unsigned(T1+T2);

                -- shift
                W(15 downto 0) := W(16 downto 1);
                W(16) := Gamma1(W(14)) + W(9) + Gamma0(W(1)) + W(0);

                
                round := round + 1;
                if round = SHA512_ROUNDS then
                    state := 4;
                else
                    state := 2;--3;
                end if;
                
            when 4 =>
                for I in 0 to 7 loop
                    sha512_digest(I) <= sha512_digest(I) + T(I);
                end loop;
                state := 0;
            when others =>
                state := 0;
            end case;                
        end if;
    end process;
    
process(sha512_digest)
begin
    for I in 0 to 7 loop
        sha512_digest_vector(I*64+63 downto I*64) <= std_logic_vector(be2le(sha512_digest(I)));
    end loop;
end process;    
    
end generate;


gen_keccak384: if CALC_KECCAK384 = 1 generate
	process (clk_slow)
		variable	state : integer range 0 to 63 := 0;
		variable round : integer range 0 to 127 := 0;
		variable C : hash_array(4 downto 0);
		variable D : hash_array(4 downto 0);
		variable hash : hash_array(SHA3_MAX_PERMUTATION_SIZE-1 downto 0);
		variable a0 : hash_type;
		variable a1 : hash_type;
		variable k : integer range 0 to 31;
		
		variable tmp : std_logic_vector(63 downto 0);
	begin
		if rising_edge(clk_slow) then
			if 1=0 then --reset_slow='0' then
				state := 0;
			else
			    if state = 0 then
			         flag_keccak384_running <= '0';
                else 
                    flag_keccak384_running <= '1';
                end if;
				case state is
					when 0 =>
						if flag_keccak384_start = '1' then
							state := 1;
						end if;
						
						if flag_keccak384_reset = '1' then
							hash := (others => (others => '0'));
						end if;
					when 1 =>	
						round := SHA3_NUM_ROUNDS;
						for I in 0 to 12 loop
                            tmp := data(I*2+1) & data(I*2+0);
							hash(I) := hash(I) xor tmp;
						end loop;
						state :=2;
					when 2 =>
						-- theta
						for I in 0 to 4 loop
							C(I) := hash(I) xor hash(I+5) xor hash(I+10) xor hash(I+15) xor hash(I+20);
						end loop;
						
						for I in 0 to 4 loop
							-- +5 for not becoming negative index for mod
							D(I) := rotl64(C((I+1) mod 5), 1) xor C((I+5-1) mod 5);
						end loop;
						
						for I in 0 to 4 loop
							for J in 0 to 4 loop
								hash(I+5*J) := hash(I+5*J) xor D(I);
							end loop;
						end loop;
						
						-- permutation
						for I in 1 to 24 loop
							hash(I) := rotl64(hash(I), rol_table(I));
						end loop;
						
						
						-- pi
						a1 := hash(1);
						hash(0) := hash(0);	-- hash(0) is left as it is
						for I in 0 to 22 loop
							hash(pi_table(I)) := hash(pi_table(I+1));
						end loop;
						hash(10) := a1;

						-- chi
						for I in 0 to 4 loop
							k := I * 5;
							a0 := hash(0 + k);
							a1 := hash(1 + k);
							hash(0 + k) := hash(0 + k) xor ((not a1) and hash(2 + k));
							hash(1 + k) := hash(1 + k) xor ((not hash(2 + k)) and hash(3 + k));
							hash(2 + k) := hash(2 + k) xor ((not hash(3 + k)) and hash(4 + k));
							hash(3 + k) := hash(3 + k) xor ((not hash(4 + k)) and a0);
							hash(4 + k) := hash(4 + k) xor ((not a0) and a1);
						end loop;
						
						hash(0) := hash(0) xor keccak_rounds(SHA3_NUM_ROUNDS-round);
						
						round := round - 1;
						if round = 0 then
							state := 3;
						end if;
					when 3 => 
						for I in 0 to 5 loop
							fast_digest((I*64)+63 downto (I*64)+0) <= hash(I);
						end loop;
						state := 0;
					when others =>
						state := 0;
				end case;
			end if;
		end if;
	end process;
end generate;

gen_troika: if CALC_TROIKA = 1 generate
    process(clk_slow)
        variable state : integer range 0 to 31;
        variable sbox_input : unsigned(5 downto 0);
        variable tmp : integer range 0 to 2047;
        variable tmp2 : std_logic_vector(53 downto 0);
        variable parity : parity;
        variable sbox : std_logic_vector(5 downto 0);
        variable row : std_logic_vector(17 downto 0);
        variable lane : std_logic_vector(53 downto 0);
        variable xm1, xp1, zp1 : integer;
        variable lfsr : state_array(26 downto 0);
        variable round : integer range 0 to 31;
        variable tmp_state : cuboid;
    begin
        if rising_edge(clk_slow) then
            if 1=0 then --reset_slow='0' then
                state := 0;
            else
                flag_troika_running <= '0';            
                if state /= 0 then
                    flag_troika_running <= '1';
                    ctr <= ctr + 1;
                end if;
                
                case state is 
                    when 0 =>
                        if flag_troika_reset = '1' then
                            troika_state <= (others => (others => (others => "00")));
                        end if;
                        
                        -- reset state above index 162 for WOTS PK MAM generation scheme
                        if flag_troika_mam_reset = '1' then
                            for Z in 6 to 26 loop
                                for Y in 0 to 2 loop
                                    for X in 0 to 8 loop
                                        troika_state(Z)(Y)(X) <= "00";
                                    end loop;
                                end loop;
                            end loop;
                        end if;
                        

                        if flag_troika_load_capacity = '1' then
                            for Z in 9 to 26 loop
                                for Y in 0 to 2 loop
                                    for X in 0 to 8 loop
                                        tmp := (Z*27+Y*9+X)*2; -- last * 2 because 2 bits per trit
                                        troika_state(Z)(Y)(X) <= data(tmp / 32)((tmp mod 32) + 1 downto (tmp mod 32) + 0);
                                    end loop;
                                end loop;
                            end loop;
                        end if;
                    
                        if flag_troika_load_rate = '1' then
                            for Z in 0 to 8 loop
                                for Y in 0 to 2 loop
                                    for X in 0 to 8 loop
                                        tmp := (Z*27+Y*9+X)*2; -- last * 2 because 2 bits per trit
                                        troika_state(Z)(Y)(X) <= data(tmp / 32)((tmp mod 32) + 1 downto (tmp mod 32) + 0);
                                    end loop;
                                end loop;
                            end loop;
                        end if;
                        
                        if flag_troika_start='1' then
                            round := 0;
                            state := 1;
                        end if;
                    when 1 =>
                        tmp_state := troika_state;
                        
                        for Z in 0 to 26 loop
                            for Y in 0 to 2 loop
                                for X in 0 to 2 loop
                                     sbox := sbox_lookup(tmp_state(Z)(Y)(X*3+2) & tmp_state(Z)(Y)(X*3+1) & tmp_state(Z)(Y)(X*3+0));
                                     tmp_state(Z)(Y)(X*3+2) := sbox(5 downto 4);
                                     tmp_state(Z)(Y)(X*3+1) := sbox(3 downto 2);
                                     tmp_state(Z)(Y)(X*3+0) := sbox(1 downto 0);
                                  end loop;
                              end loop;
                          end loop;
    
                        for Z in 0 to 26 loop
                            for Y in 0 to 2 loop
                                for X in 0 to 8 loop
                                    row(X*2+1 downto X*2+0) := tmp_state(Z)(Y)(X);
                                end loop;
                                row := rol_row(row, row_table(Y)*2);
                                for X in 0 to 8 loop
                                    tmp_state(Z)(Y)(X) := row(X*2+1 downto X*2+0);
                                end loop;
                            end loop;
                        end loop;
    
                        for X in 0 to 8 loop
                            for Y in 0 to 2 loop
                                for Z in 0 to 26 loop
                                    lane(Z*2+1 downto Z*2+0) := tmp_state(Z)(Y)(X);
                                end loop;
                                lane := rol_lane(lane, lane_table(Y*9+X)*2);
                                -- transpose back
                                for Z in 0 to 26 loop
                                    tmp_state(Z)(Y)(X) := lane(Z*2+1 downto Z*2+0);
                                end loop;
                            end loop;
                        end loop;
    
                        for Z in 0 to 26 loop
                            for X in 0 to 8 loop
                                parity(Z)(X) := trit_add(tmp_state(Z)(0)(X), trit_add(tmp_state(Z)(1)(X), tmp_state(Z)(2)(X))); 
                            end loop;
                        end loop;
                        
                        for Z in 0 to 26 loop
                            for X in 0 to 8 loop
                                for Y in 0 to 2 loop
                                    tmp_state(Z)(Y)(X) := trit_add(tmp_state(Z)(Y)(X), trit_add(parity(Z)((X+9-1) mod 9), parity((Z+1) mod 27)((X+1) mod 9))); 
                                end loop;
                             end loop;
                         end loop;
    
                        for Z in 0 to 26 loop
                            for Y in 1 to 2 loop
                                for X in 0 to 8 loop
                                    troika_state(Z)(Y)(X) <= tmp_state(Z)(Y)(X);
                                end loop;
                            end loop;
                        end loop;
                        
                        for I in 0 to 26 loop
                            lfsr(I) := const_lfsr(round, I);
                        end loop;
    
                        for Z in 0 to 26 loop
                            for X in 0 to 8 loop  
                                troika_state(Z)(0)(X) <= trit_add(tmp_state(Z)(0)(X), lfsr(Z)(1 downto 0));
                                lfsr(Z)(11*2+1 downto 11*2+0) := trit_sub(lfsr(Z)(3*2+1 downto 3*2+0), lfsr(Z)(1 downto 0));
                                lfsr(Z) := "00" & lfsr(Z)(31 downto 2);
                            end loop;
                        end loop;
                        round := round + 1;
                        if round = troika_num_rounds then -- finished, export state
                             state := 0;                        
                        end if;                        
                    when others =>
                        state := 0;
                end case;
            end if;
        end if;
    end process;
    -- don't use additional flipflops for hash - do something like an C union instead^^
    process(troika_state)
    variable tmp : integer range 0 to 2047;
    begin
        for Z in 0 to 26 loop --8 loop
           for Y in 0 to 2 loop
               for X in 0 to 8 loop
                   tmp := (Z*27+Y*9+X)*2; -- last * 2 because 2 bits per trit
                   hash(tmp / 32)((tmp mod 32) + 1 downto (tmp mod 32) + 0) <= troika_state(Z)(Y)(X);
                end loop;
            end loop;
        end loop;    
    end process;
    
end generate;	
	
gen_pow: if CALC_POW = 1 generate
	process (clk_fast)
		variable state : integer range 0 to 63 := 0;
		variable round : integer range 0 to 127 := 0;
		
		variable i_min_weight_magnitude : min_weight_magnitude_type;

        -- temporary registers get optimized away
        variable alpha : curl_state_array(STATE_LENGTH-1 downto 0);
        variable beta : curl_state_array(STATE_LENGTH-1 downto 0);
        variable gamma : curl_state_array(STATE_LENGTH-1 downto 0);
        variable delta : curl_state_array(STATE_LENGTH-1 downto 0);

		variable tmp_highest_bit : integer range 0 to 31;
		variable tmp_index : integer range 0 to 728;
		
		variable mode : integer range 0 to 1;
		
	begin
		if rising_edge(clk_fast) then
            case state is
                when 0 =>
                    flag_running_fast <= '0';
                when others =>
                    flag_running_fast <= '1';
            end case;
            
            case state is
                when 0 =>
                    if flag_start(2) = '1' then
                        i_binary_nonce <= x"00000000";

                        imask <= (others => '0');   -- fix for small MWM
                        tmp_weight_magnitude <= (others => (others => '0'));

                        i_min_weight_magnitude := min_weight_magnitude;

                        mode := 0;
                        state := 1;
                    end if;
                    
                    if flag_curl_do_curl(2) = '1' then
                        mode := 1;
                        state := 1;
                    end if;
                    
                    
                    if flag_curl_reset(2) = '1' then
                        curl_mid_state_low(STATE_LENGTH-1 downto HASH_LENGTH) <= (others => '1');
                        curl_mid_state_high(STATE_LENGTH-1 downto HASH_LENGTH) <= (others => '1');
                    end if;
                    
                -- do PoW
                when 1 =>	-- copy mid state and insert nonce
                    flag_found_fast <= '0';
                    flag_overflow_fast <= '0';
                    binary_nonce <= i_binary_nonce;						
                    -- pipelining
                    i_binary_nonce <= i_binary_nonce + 1;

                    for I in HASH_LENGTH to STATE_LENGTH-1 loop
                        curl_state_low(I) <= expand(curl_mid_state_low(I));
                        curl_state_high(I) <= expand(curl_mid_state_high(I));
                    end loop;

                    for I in 0 to HASH_LENGTH-1 loop
                        curl_state_low(I) <= expand(data((I*2)/32)((I*2) mod 32));
                        curl_state_high(I) <= expand(data((I*2)/32)((I*2) mod 32 + 1));
                    end loop;
                
                    round := NUMBER_OF_ROUNDS;
                    state := 2;

                    if mode = 0 then
                        
                        -- calculate log2(x) by determining the place of highest set bit
                        -- this is calculated on constants, so no logic needed
                        tmp_highest_bit := 0;
                        for I in 0 to 31 loop	-- 32 is enough ...^^
                            if to_unsigned(PARALLEL-1, 32)(I) = '1' then
                                tmp_highest_bit := I;
                            end if;
                        end loop;
    
                        -- insert 
                        for J in 0 to 23 loop
                            curl_state_low(NONCE_OFFSET+J) <= expand(signature(J)(0));
                            curl_state_high(NONCE_OFFSET+J) <= expand(signature(J)(1));
                        end loop;
    
--						-- generate bitmuster in first trit-arrays of nonce depending on PARALLEL setting
                        -- this is calculated on constants, so no logic needed
                        for I in 0 to PARALLEL-1 loop
                            for J in 0 to tmp_highest_bit loop
                                curl_state_low(NONCE_OFFSET+J+24)(I) <= to_unsigned(I, tmp_highest_bit+1)(J);
                                curl_state_high(NONCE_OFFSET+J+24)(I) <= not to_unsigned(I, tmp_highest_bit+1)(J);
                            end loop;
                        end loop;
                        
                        -- insert and convert binary nonce to ternary nonce
                        -- It's a fake ternary nonce but integer-values are strictly monotonously rising 
                        -- with integer values of binary nonce.
                        -- Doesn't bring the exact same result like reference implementation with real
                        -- ternary adder - but it doesn't matter and it is way faster.
                        -- conveniently put nonce counter at the end of nonce
                        for I in 0 to INTERN_NONCE_LENGTH-1 loop
                            curl_state_low(NONCE_OFFSET + NONCE_LENGTH - INTERN_NONCE_LENGTH + I) <= expand(i_binary_nonce(I));
                            curl_state_high(NONCE_OFFSET + NONCE_LENGTH - INTERN_NONCE_LENGTH + I) <= not expand(i_binary_nonce(I));
                        end loop;

                        -- initialize round-counter
                        if i_binary_nonce = x"ffffffff" or ff_stop = '1' then
                            flag_overflow_fast <= '1';
                            state := 0;
                        end if;
                    end if;

                when 2 =>	-- do the curl hash round without any copying needed
                    if round = 1 then
                        state := 3;
                    end if;

                    for I in 0 to STATE_LENGTH-1 loop
                        alpha(I) := curl_state_low(index_table(I));
                        beta(I) := curl_state_high(index_table(I));
                        gamma(I) := curl_state_high(index_table(I+1));
                                                
                        delta(I) := (alpha(I) or (not gamma(I))) and (curl_state_low(index_table(I+1)) xor beta(I));
                    
                        curl_state_low(I) <= not delta(I);
                        curl_state_high(I) <= (alpha(I) xor gamma(I)) or delta(I);
                    end loop;
                    
                    round := round - 1;
                when 3 =>  -- find out which solution - if any
                    if mode = 0 then
                        -- transform "vertical" trits to "horizontal" bits
                        -- and compare with min weight magnitude mask
                        for I in 0 to PARALLEL-1 loop
                            for J in 0 to BITS_MIN_WEIGHT_MAGINUTE_MAX-1 loop
                                tmp_weight_magnitude(I)(J) <= curl_state_low(HASH_LENGTH - 1 - J)(I) and curl_state_high(HASH_LENGTH - 1 - J)(I) and i_min_weight_magnitude(J);
                            end loop;
                        end loop;

                        -- pipelining
                        imask <= (others => '0');
                        for I in 0 to PARALLEL-1 loop
                            if tmp_weight_magnitude(I) = i_min_weight_magnitude then
                                imask(I) <= '1';
                            end if;
                        end loop;
                    
                        -- pipelining
                        if unsigned(imask) = 0 then
                            state :=1;
                        else
                            flag_found_fast <= '1';
                            mask <= imask;
                            state :=0;
                        end if;
                    else
                        state := 4;
                    end if;
                when 4 =>
                    for I in STATE_LENGTH-1 downto 0 loop
                        curl_mid_state_low(I) <= curl_state_low(I)(0);
                        curl_mid_state_high(I) <= curl_state_high(I)(0);
                    end loop;
                    state := 0;
                when others =>
                    state := 0;
            end case;
        end if;
	end process;
	
    -- don't use additional flipflops for hash - do something like an C union instead^^
    process(curl_mid_state_low, curl_mid_state_high)
    variable tmp : integer range 0 to 1023;
    begin
        for I in 0 to HASH_LENGTH-1 loop
            curl_hash((I*2)/32)((I*2) mod 32) <= curl_mid_state_low(I);
            curl_hash((I*2)/32)((I*2) mod 32 + 1) <= curl_mid_state_high(I);
        end loop;            
    end process;
	
	
end generate;	
end behv;
