set_property INTERNAL_VREF 0.675 [get_iobanks 34]

#create_clock -period 10.000 -name clk -waveform {0.000 5.000} [get_ports clk]

# done
set_property PACKAGE_PIN G11 [get_ports sys_clock]
set_property IOSTANDARD LVCMOS33 [get_ports sys_clock]

set_property PACKAGE_PIN L13 [get_ports iic_scl_io]
set_property PACKAGE_PIN L14 [get_ports iic_sda_io]

set_property IOSTANDARD LVCMOS33 [get_ports iic_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports iic_sda_io]

set_property PACKAGE_PIN L12 [get_ports power_down]
set_property IOSTANDARD LVCMOS33 [get_ports power_down]
set_property PULLDOWN true [get_ports power_down]

set_property PACKAGE_PIN M1 [get_ports reset]
set_property PACKAGE_PIN M2 [get_ports tms_ext]
set_property PACKAGE_PIN J2 [get_ports tdi_ext]
set_property PACKAGE_PIN H2 [get_ports tck_ext]
set_property PACKAGE_PIN J1 [get_ports tdo_ext]

set_property IOSTANDARD LVCMOS33 [get_ports reset]
set_property IOSTANDARD LVCMOS33 [get_ports tms_ext]
set_property IOSTANDARD LVCMOS33 [get_ports tdi_ext]
set_property IOSTANDARD LVCMOS33 [get_ports tck_ext]
set_property IOSTANDARD LVCMOS33 [get_ports tdo_ext]

set_property PULLUP true [get_ports tms_ext]
set_property PULLUP true [get_ports tdi_ext]
set_property PULLUP true [get_ports tck_ext]
set_property PULLUP true [get_ports reset]


# mosi 0
set_property PACKAGE_PIN L1 [get_ports {gpios[0]}]
# miso 0
set_property PACKAGE_PIN L2 [get_ports {gpios[1]}]
# sck 0
set_property PACKAGE_PIN P2 [get_ports {gpios[2]}]
# ss 0
set_property PACKAGE_PIN M4 [get_ports {gpios[3]}]
# ss 1
set_property PACKAGE_PIN P3 [get_ports {gpios[4]}]
# scl0
set_property PACKAGE_PIN P12 [get_ports {gpios[5]}]
# sda0
set_property PACKAGE_PIN M12 [get_ports {gpios[6]}]
# rx
set_property PACKAGE_PIN N4 [get_ports {gpios[7]}]
# tx
set_property PACKAGE_PIN P4 [get_ports {gpios[8]}]
# rest general purpose
set_property PACKAGE_PIN P10 [get_ports {gpios[9]}]
set_property PACKAGE_PIN N10 [get_ports {gpios[10]}]
set_property PACKAGE_PIN M10 [get_ports {gpios[11]}]
set_property PACKAGE_PIN P11 [get_ports {gpios[12]}]
set_property PACKAGE_PIN N11 [get_ports {gpios[13]}]
set_property PACKAGE_PIN M11 [get_ports {gpios[14]}]
set_property PACKAGE_PIN P13 [get_ports {gpios[15]}]
set_property PACKAGE_PIN M13 [get_ports {gpios[16]}]
set_property PACKAGE_PIN N14 [get_ports {gpios[17]}]
set_property PACKAGE_PIN M14 [get_ports {gpios[18]}]
# led user
set_property PACKAGE_PIN A13 [get_ports {gpios[19]}]
# led lock
set_property PACKAGE_PIN A12 [get_ports {gpios[20]}]

set_property IOSTANDARD LVCMOS33 [get_ports {gpios[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpios[20]}]

set_property PACKAGE_PIN B11 [get_ports qspi_flash_io0_io]
set_property PACKAGE_PIN B12 [get_ports qspi_flash_io1_io]
set_property PACKAGE_PIN D10 [get_ports qspi_flash_io2_io]
set_property PACKAGE_PIN C10 [get_ports qspi_flash_io3_io]
set_property PACKAGE_PIN C11 [get_ports {qspi_flash_ss_io[0]}]


# not done
#set_property BOARD_PIN {reset} [get_ports ext_reset_in]


set_property IOSTANDARD LVCMOS33 [get_ports qspi_flash_io0_io]
set_property IOSTANDARD LVCMOS33 [get_ports qspi_flash_io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports qspi_flash_io2_io]
set_property IOSTANDARD LVCMOS33 [get_ports qspi_flash_io3_io]
set_property IOSTANDARD LVCMOS33 [get_ports {qspi_flash_ss_io[0]}]

set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 3 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DISABLE [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
#set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]




##encryption settings
#set_property BITSTREAM.ENCRYPTION.ENCRYPT YES [current_design]
##set_property BITSTREAM.ENCRYPTION.ENCRYPTKEYSELECT BBRAM [current_design]
#set_property BITSTREAM.ENCRYPTION.ENCRYPTKEYSELECT eFUSE [current_design]
#set_property BITSTREAM.ENCRYPTION.KEY0 256'h78214125442A472D4B614E645267556B58703273357638792F423F4528482B4D [current_design]

create_generated_clock -name cpu_clk [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name qspi_clk [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT1]

create_clock -period 100.000 -name tck_ext [get_ports tck_ext]
create_clock -period 83.333 -name sys_clock -waveform {0.000 41.667} [get_ports sys_clock]
set_clock_groups -name clock_groups -asynchronous -group [get_clocks [get_clocks -of_objects [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0]]] -group [get_clocks [get_clocks -of_objects [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT2]]] -group [get_clocks tck_ext]
set_input_delay -clock [get_clocks cpu_clk] -add_delay 0.500 [get_ports reset*]

##########################################
#
create_generated_clock -name cclk -source [get_pins -hier -regexp design_iccfpga_i/axi_quad_spi_flash/.*STARTUP.*/USRCCLKO] -combinational [get_pins -hier -regexp design_iccfpga_i/axi_quad_spi_flash/.*STARTUP.*/USRCCLKO]

set_clock_latency -min 0.500 [get_clocks cclk]
set_clock_latency -max 6.700 [get_clocks cclk]

set_max_delay -datapath_only -from [get_clocks cpu_clk] -to [get_clocks -include_generated_clocks qspi_clk] 8.500
set_max_delay -datapath_only -from [get_clocks -include_generated_clocks qspi_clk] -to [get_clocks cpu_clk] 17.000

# cclk is independent of cpu_clk and qspi_clk, due to large offset skews
set_max_delay -datapath_only -from [get_clocks cclk] -to [get_clocks -include_generated_clocks qspi_clk] 8.500
set_max_delay -datapath_only -from [get_clocks -include_generated_clocks qspi_clk] -to [get_clocks cclk] 8.500

set_max_delay -datapath_only -from [get_clocks cclk] -to [get_clocks cpu_clk] 9.500
set_max_delay -datapath_only -from [get_clocks cpu_clk] -to [get_clocks cclk] 9.500



set_input_delay -clock [get_clocks cclk] -max -add_delay 7.500 [get_ports qspi_flash_io?_io]
set_input_delay -clock [get_clocks cclk] -min -add_delay 1.500 [get_ports qspi_flash_io?_io]
set_input_delay -clock [get_clocks cclk] -max -add_delay 7.500 [get_ports qspi_flash_ss*]
set_input_delay -clock [get_clocks cclk] -min -add_delay 1.500 [get_ports qspi_flash_ss*]

set_output_delay -clock [get_clocks cclk] -max -add_delay 2.500 [get_ports qspi_flash_io?_io]
set_output_delay -clock [get_clocks cclk] -min -add_delay -3.500 [get_ports qspi_flash_io?_io]
set_output_delay -clock [get_clocks cclk] -max -add_delay 2.500 [get_ports qspi_flash_ss*]
set_output_delay -clock [get_clocks cclk] -min -add_delay -3.500 [get_ports qspi_flash_ss*]
#

set_input_delay -clock [get_clocks cpu_clk] -add_delay 0.500 [get_ports reset*]
set_false_path -from [get_ports reset*] -to [get_clocks qspi_clk]


############################
# QSPI Flash
############################
# Following are the SPI device parameters
# Max Tco
# Min Tco
# Setup time requirement
# Hold time requirement
# Following are the board/trace delay numbers
# Assumption is that all Data lines are matched
### End of user provided delay numbers
# This is to ensure min routing delay from SCK generation to STARTUP input
# User should change this value based on the results
# Having more delay on this net reduces the Fmax
# Following constraint should be commented when the STARTUP block is disabled
#set_max_delay 1.5 -from [get_pins -hier *SCK_O_reg_reg/C] -to [get_pins -hier *USRCCLKO] -datapath_only

#set_max_delay -from [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*SCK_O_reg_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*USRCCLKO}] 1.500
#set_min_delay -from [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*SCK_O_reg_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*USRCCLKO}] 0.100

# not sure if this matches only what it has to match!
#set_max_delay -datapath_only -from [get_pins -hier *SCK_O_reg_reg/C] -to [get_pins -hier *USRCCLKO] 1.500
#set_min_delay -from [get_pins -hier *SCK_O_reg_reg/C] -to [get_pins -hier *USRCCLKO] 0.100



# Following command creates a divide by 2 clock
# It also takes into account the delay added by the STARTUP block to route the CCLK
# This constraint is not needed when the STARTUP block is disabled
# The following constraint should be commented when the STARTUP block is disabled
#create_generated_clock -name clk_sck -source [get_pins -hierarchical *axi_quad_spi_flash/ext_spi_clk] -edges {3 5 7} -edge_shift {6.700 6.700 6.700} [get_pins -hierarchical *USRCCLKO]]

#create_generated_clock -name clk_sck -source        [get_pins -hier -regexp design_iccfpga_i/axi_quad_spi_flash/.*STARTUP2.*/USRCCLKO] #                                  -combinational [get_pins -hier -regexp design_iccfpga_i/axi_quad_spi_flash/.*STARTUP2.*/USRCCLKO]

# Data is captured into FPGA on the second rising edge of ext_spi_clk after the SCK falling edge
# Data is driven by the FPGA on every alternate rising_edge of ext_spi_clk
#set_input_delay -clock clk_sck -clock_fall -max 7.450 [get_ports qspi_flash_io?_io]
#set_input_delay -clock clk_sck -clock_fall -min 1.450 [get_ports qspi_flash_io?_io]
#set_input_delay -clock clk_sck -clock_fall -max 7.450 [get_ports qspi_flash_ss_io*]
#set_input_delay -clock clk_sck -clock_fall -min 1.450 [get_ports qspi_flash_ss_io*]
#set_multicycle_path -setup -from clk_sck -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*ext_spi_clk}]] 2
#set_multicycle_path -hold -end -from clk_sck -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*ext_spi_clk}]] 1
# Data is captured into SPI on the following rising edge of SCK
# Data is driven by the IP on alternate rising_edge of the ext_spi_clk
#set_output_delay -clock clk_sck -max 2.050 [get_ports qspi_flash_io?_io]
#set_output_delay -clock clk_sck -min -2.950 [get_ports qspi_flash_io?_io]
#set_output_delay -clock clk_sck -max 2.050 [get_ports qspi_flash_ss_io*]
#set_output_delay -clock clk_sck -min -2.950 [get_ports qspi_flash_ss_io*]
#set_multicycle_path -setup -start -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*ext_spi_clk}]] -to clk_sck 2
#set_multicycle_path -hold -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *axi_quad_spi_flash*ext_spi_clk}]] -to clk_sck 1


set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
