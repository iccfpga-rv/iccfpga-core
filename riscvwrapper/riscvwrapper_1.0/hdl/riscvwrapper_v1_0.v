
`timescale 1 ns / 1 ps

	module riscvwrapper_v1_0 #
	(
		// Users to add parameters here
        parameter integer lock = 0,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Master Bus Interface M00_AXI
		parameter  C_M00_AXI_TARGET_SLAVE_BASE_ADDR	= 32'h40000000,
		parameter integer C_M00_AXI_BURST_LEN	= 16,
		parameter integer C_M00_AXI_ID_WIDTH	= 1,
		parameter integer C_M00_AXI_ADDR_WIDTH	= 32,
		parameter integer C_M00_AXI_DATA_WIDTH	= 32,
		parameter integer C_M00_AXI_AWUSER_WIDTH	= 0,
		parameter integer C_M00_AXI_ARUSER_WIDTH	= 0,
		parameter integer C_M00_AXI_WUSER_WIDTH	= 0,
		parameter integer C_M00_AXI_RUSER_WIDTH	= 0,
		parameter integer C_M00_AXI_BUSER_WIDTH	= 0

	)
	(
    	
	    input   reset,
        input   clk,
        input   jtag_tms,
        input   jtag_tdi,
        output  jtag_tdo,
        input   jtag_tck,
        output  locked,
        

//        input   timer_clear,
//        input   timer_tick,
        input   timer_interrupt,
        input   ext_interrupt,
        
        
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Master Bus Interface M00_AXI
//		input wire  m00_axi_init_axi_txn,
//		output wire  m00_axi_txn_done,
//		output wire  m00_axi_error,
//		input wire  m00_axi_aclk,
//		input wire  m00_axi_aresetn,
		output wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_awid,
		output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr,
		output wire [7 : 0] m00_axi_awlen,
		output wire [2 : 0] m00_axi_awsize,
		output wire [1 : 0] m00_axi_awburst,
		output wire  m00_axi_awlock,
		output wire [3 : 0] m00_axi_awcache,
		output wire [2 : 0] m00_axi_awprot,
		output wire [3 : 0] m00_axi_awqos,
		output wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m00_axi_awuser,
		output wire  m00_axi_awvalid,
		input wire  m00_axi_awready,
		output wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata,
		output wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb,
		output wire  m00_axi_wlast,
		output wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m00_axi_wuser,
		output wire  m00_axi_wvalid,
		input wire  m00_axi_wready,
		input wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_bid,
		input wire [1 : 0] m00_axi_bresp,
		input wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m00_axi_buser,
		input wire  m00_axi_bvalid,
		output wire  m00_axi_bready,
		output wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_arid,
		output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr,
		output wire [7 : 0] m00_axi_arlen,
		output wire [2 : 0] m00_axi_arsize,
		output wire [1 : 0] m00_axi_arburst,
		output wire  m00_axi_arlock,
		output wire [3 : 0] m00_axi_arcache,
		output wire [2 : 0] m00_axi_arprot,
		output wire [3 : 0] m00_axi_arqos,
		output wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m00_axi_aruser,
		output wire  m00_axi_arvalid,
		input wire  m00_axi_arready,
		input wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_rid,
		input wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata,
		input wire [1 : 0] m00_axi_rresp,
		input wire  m00_axi_rlast,
		input wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m00_axi_ruser,
		input wire  m00_axi_rvalid,
		output wire  m00_axi_rready
	);
	
generate
    if (lock == 0)
        assign locked = 1'b0;
    else
        assign locked = 1'b1;
endgenerate

generate
    if (lock == 0) 
    begin
        ICCFPGA ICCFPGA_inst (
            .io_axiIO_aw_valid(m00_axi_awvalid),
            .io_axiIO_aw_ready(m00_axi_awready),
            
            .io_axiIO_aw_payload_addr(m00_axi_awaddr),
            .io_axiIO_aw_payload_id(m00_axi_awid),
            // region
            .io_axiIO_aw_payload_len(m00_axi_awlen),
            .io_axiIO_aw_payload_size(m00_axi_awsize),
            .io_axiIO_aw_payload_burst(m00_axi_awburst),
            .io_axiIO_aw_payload_lock(m00_axi_awlock),
            .io_axiIO_aw_payload_cache(m00_axi_awcache),
            .io_axiIO_aw_payload_qos(m00_axi_awqos),
            .io_axiIO_aw_payload_prot(m00_axi_awprot),
            
            //        .M_AXI_AWUSER(m00_axi_awuser),
            .io_axiIO_w_valid(m00_axi_wvalid),
            .io_axiIO_w_ready(m00_axi_wready),
            
            .io_axiIO_w_payload_data(m00_axi_wdata),
            .io_axiIO_w_payload_strb(m00_axi_wstrb),
            .io_axiIO_w_payload_last(m00_axi_wlast),
            
            //        .M_AXI_WUSER(m00_axi_wuser),
            .io_axiIO_b_valid(m00_axi_bvalid),
            .io_axiIO_b_ready(m00_axi_bready),
            
            .io_axiIO_b_payload_id(m00_axi_bid),
            .io_axiIO_b_payload_resp(m00_axi_bresp),
            
            //        .M_AXI_BUSER(m00_axi_buser),
            .io_axiIO_ar_valid(m00_axi_arvalid),
            .io_axiIO_ar_ready(m00_axi_arready),
            
            .io_axiIO_ar_payload_addr(m00_axi_araddr),
            .io_axiIO_ar_payload_id(m00_axi_arid),
            // region            
            .io_axiIO_ar_payload_len(m00_axi_arlen),
            .io_axiIO_ar_payload_size(m00_axi_arsize),
            .io_axiIO_ar_payload_burst(m00_axi_arburst),
            .io_axiIO_ar_payload_lock(m00_axi_arlock),
            .io_axiIO_ar_payload_cache(m00_axi_arcache),
            .io_axiIO_ar_payload_qos(m00_axi_arqos),
            .io_axiIO_ar_payload_prot(m00_axi_arprot),
            //        .M_AXI_ARUSER(m00_axi_aruser),
            
            .io_axiIO_r_valid(m00_axi_rvalid),
            .io_axiIO_r_ready(m00_axi_rready),
            
            .io_axiIO_r_payload_data(m00_axi_rdata),
            .io_axiIO_r_payload_id(m00_axi_rid),
            .io_axiIO_r_payload_resp(m00_axi_rresp),
            .io_axiIO_r_payload_last(m00_axi_rlast),
    //        .M_AXI_RUSER(m01_axi_ruser),
                      
            .io_jtag_tms(jtag_tms),
            .io_jtag_tdi(jtag_tdi),
            .io_jtag_tdo(jtag_tdo),
            .io_jtag_tck(jtag_tck),
            .io_axiClk(clk),
            .io_asyncReset(reset),
    //        .io_timerExternal_clear(timer_clear),
    //        .io_timerExternal_tick(timer_tick),
            .io_coreInterrupt(ext_interrupt),
            .io_timerInterrupt(timer_interrupt)
        );
    end else
    begin
        NoDebug_ICCFPGA ICCFPGA_inst (
            .io_axiIO_aw_valid(m00_axi_awvalid),
            .io_axiIO_aw_ready(m00_axi_awready),
            
            .io_axiIO_aw_payload_addr(m00_axi_awaddr),
            .io_axiIO_aw_payload_id(m00_axi_awid),
            // region
            .io_axiIO_aw_payload_len(m00_axi_awlen),
            .io_axiIO_aw_payload_size(m00_axi_awsize),
            .io_axiIO_aw_payload_burst(m00_axi_awburst),
            .io_axiIO_aw_payload_lock(m00_axi_awlock),
            .io_axiIO_aw_payload_cache(m00_axi_awcache),
            .io_axiIO_aw_payload_qos(m00_axi_awqos),
            .io_axiIO_aw_payload_prot(m00_axi_awprot),
            
            //        .M_AXI_AWUSER(m00_axi_awuser),
            .io_axiIO_w_valid(m00_axi_wvalid),
            .io_axiIO_w_ready(m00_axi_wready),
            
            .io_axiIO_w_payload_data(m00_axi_wdata),
            .io_axiIO_w_payload_strb(m00_axi_wstrb),
            .io_axiIO_w_payload_last(m00_axi_wlast),
            
            //        .M_AXI_WUSER(m00_axi_wuser),
            .io_axiIO_b_valid(m00_axi_bvalid),
            .io_axiIO_b_ready(m00_axi_bready),
            
            .io_axiIO_b_payload_id(m00_axi_bid),
            .io_axiIO_b_payload_resp(m00_axi_bresp),
            
            //        .M_AXI_BUSER(m00_axi_buser),
            .io_axiIO_ar_valid(m00_axi_arvalid),
            .io_axiIO_ar_ready(m00_axi_arready),
            
            .io_axiIO_ar_payload_addr(m00_axi_araddr),
            .io_axiIO_ar_payload_id(m00_axi_arid),
            // region            
            .io_axiIO_ar_payload_len(m00_axi_arlen),
            .io_axiIO_ar_payload_size(m00_axi_arsize),
            .io_axiIO_ar_payload_burst(m00_axi_arburst),
            .io_axiIO_ar_payload_lock(m00_axi_arlock),
            .io_axiIO_ar_payload_cache(m00_axi_arcache),
            .io_axiIO_ar_payload_qos(m00_axi_arqos),
            .io_axiIO_ar_payload_prot(m00_axi_arprot),
            //        .M_AXI_ARUSER(m00_axi_aruser),
            
            .io_axiIO_r_valid(m00_axi_rvalid),
            .io_axiIO_r_ready(m00_axi_rready),
            
            .io_axiIO_r_payload_data(m00_axi_rdata),
            .io_axiIO_r_payload_id(m00_axi_rid),
            .io_axiIO_r_payload_resp(m00_axi_rresp),
            .io_axiIO_r_payload_last(m00_axi_rlast),
    //        .M_AXI_RUSER(m01_axi_ruser),
                      
            .io_jtag_tms(jtag_tms),
            .io_jtag_tdi(jtag_tdi),
            .io_jtag_tdo(jtag_tdo),
            .io_jtag_tck(jtag_tck),
            .io_axiClk(clk),
            .io_asyncReset(reset),
    //        .io_timerExternal_clear(timer_clear),
    //        .io_timerExternal_tick(timer_tick),
            .io_coreInterrupt(ext_interrupt),
            .io_timerInterrupt(timer_interrupt)
        );

    end
endgenerate    


endmodule
